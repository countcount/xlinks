mod db;
mod extract;
mod filters;
mod namespaces;
mod util;

use std::fs::read_to_string;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::str::FromStr;
use std::time::Duration;

use anyhow::{Result, anyhow, ensure};
use askama::Template;
use axum::extract::{Query, State};
use axum::http::{StatusCode, Uri};
use axum::response::{Html, IntoResponse, Response};
use axum::{Router, routing};
use cidr::IpInet;
use db::AddedUrl;
use extract::{ExtractReferer, ExtractUserAgent};
use futures_util::TryStreamExt;
use itertools::Itertools;
use namespaces::NamespacesCache;
use sbl::SblCheck;
use serde::Deserialize;
use sqlx::mysql::{MySqlConnectOptions, MySqlPoolOptions};
use sqlx::{ConnectOptions, MySql, Pool};
use thiserror::Error;
use time::OffsetDateTime;
use tokio::task;

use tower_http::trace::TraceLayer;
use tracing::level_filters::LevelFilter;
use tracing::log::info;
use tracing::{error, instrument, warn};
use tracing_subscriber::prelude::*;
use util::user_agent;
use wikimedia_events::page_change::PageChange;

use crate::db::{
    QueryBy, RevisionLinks, get_added_links, get_global_user_id, get_last_event_times,
};
use replica_pools::ReplicaPools;
use shared::api_client::{
    ApiClientService, new_burstable_action_api_client_service, new_sbl_api_client_service,
};
use shared::normalize_host;

fn init_logging() {
    let filter = tracing_subscriber::filter::Targets::new()
        .with_default(tracing::Level::INFO)
        .with_target("eventsource_client::client", LevelFilter::WARN)
        .with_target("wikimedia_eventstreams", LevelFilter::WARN);

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();
}

#[derive(Debug, serde::Deserialize)]
struct ReplicaPoolsConfig {
    urls: [String; 8],
    username: String,
    password: String,
}

#[derive(Debug, serde::Deserialize)]
struct Config {
    site: String,
    local_db_url: String,
    replicas: ReplicaPoolsConfig,
}

fn load_config() -> Config {
    toml::from_str::<Config>(
        &read_to_string("spamcheck.toml").expect("spamcheck.toml should exist"),
    )
    .expect("spamcheck.toml should be valid")
}

#[derive(Clone)]
struct AppState {
    pool: Pool<MySql>,
    replica_pools: ReplicaPools,
    action_acs: ApiClientService,
    nss_cache: NamespacesCache,
    sblc: sbl::SblCheck,
    site: String,
}

#[derive(Error, Debug)]
enum AppError {
    #[error("{0}")]
    BadRequest(&'static str),
    #[error("Internal Error: {0}")]
    InternalError(#[from] anyhow::Error),
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        match self {
            Self::BadRequest(s) => (StatusCode::BAD_REQUEST, s.to_string()),
            Self::InternalError(e) => (StatusCode::INTERNAL_SERVER_ERROR, format!("{e}")),
        }
        .into_response()
    }
}

#[derive(Template)]
#[template(path = "list.html")]
struct ListTemplate<'a> {
    site: &'a str,
    user_query: bool,
    query: &'a str,
    revision_links: Vec<(String, Vec<RevisionLinks>)>,
    sbl_hits: Vec<sbl::Hit>,
    verbose_links: bool,
    receive_backlogged: Option<time::Duration>,
}

#[derive(Deserialize)]
struct QueryParams {
    q: String,
    verbose: Option<String>,
}
#[instrument(name = "health", err(Debug), skip_all)]
async fn health_check(State(state): State<AppState>) -> Result<&'static str, AppError> {
    let added_links = get_added_links(
        &QueryBy::Domain("spamchecktest.example.org"),
        1000,
        &state.pool,
        &state.replica_pools,
        &state.action_acs,
        state.nss_cache.clone(),
    )
    .await?;

    (|| {
        ensure!(
            added_links.rev_links.len() == 1,
            "{} != 1 links found",
            added_links.rev_links.len()
        );
        let rev_links = &added_links.rev_links[0];
        let expected_rev_links = &RevisionLinks {
            domain: "en.wikipedia.org".to_string(),
            revision: 1241456139,
            timestamp: OffsetDateTime::from_unix_timestamp(1724227545)?,
            user: db::User::Global("Count Count".to_string()),
            bot: false,
            minor_edit: false,
            new_page: false,
            reverted: true,
            page_deleted: false,
            allowlisted: false,
            host_added_count: 1,
            host_removed_count: 1,
            page_title: "User:Count_Count/sandbox".to_string(),
            urls: vec![AddedUrl {
                url: "https://spamchecktest.example.org/health-check".to_string(),
                domain: "spamchecktest.example.org".to_string(),
                still_present: false,
            }],
        };
        ensure!(
            rev_links == expected_rev_links,
            "Links do not match - expected: {:?}, actual: {:?}",
            expected_rev_links,
            rev_links
        );
        Ok(())
    })()?;

    Ok("Ok")
}

#[instrument(name = "list", err(Debug), skip_all, fields(uri = uri.to_string(), referer, ua=ua))]
async fn list_by_domain(
    params: Query<QueryParams>,
    State(state): State<AppState>,
    uri: Uri,
    ExtractReferer(referer): ExtractReferer,
    ExtractUserAgent(ua): ExtractUserAgent,
) -> Result<Html<String>, AppError> {
    if is_disguised_crawler(&referer, &ua, &uri, &state) {
        return empty_result(&state, "");
    }

    if !referer.is_empty() {
        tracing::Span::current().record("referer", &referer);
    }

    let mut d = params.q.as_str().trim();
    if d.starts_with('.') {
        d = &d[1..];
    }
    if d.is_empty() {
        return Err(AppError::BadRequest("The domain name is empty."));
    }
    if d.chars()
        .any(|c| c.is_whitespace() || matches!(c, ':' | '/' | '%' | '#' | '?' | '_' | '\\' | '*'))
    {
        return Err(AppError::BadRequest(
            "The domain name contains invalid characters.",
        ));
    }

    let verbose = params
        .verbose
        .as_deref()
        .is_some_and(|v| v != "0" && !v.eq_ignore_ascii_case("false"));

    let time = std::time::Instant::now();
    let nd = normalize_host(d);
    let d = match nd.as_ref() {
        Some(nd) => nd,
        None => d,
    };
    let receive_backlogged = get_backlogged(&state).await?;

    let added_links = get_added_links(
        &QueryBy::Domain(d),
        1000,
        &state.pool,
        &state.replica_pools,
        &state.action_acs,
        state.nss_cache.clone(),
    )
    .await?;
    let rev_links = group_by_wiki(added_links);
    log_elapsed(time.elapsed(), is_bot(&ua));

    let sbl_hits = state
        .sblc
        .get_hits(&format!("https://{d}"))
        .map_err(|e| anyhow!(e))?;

    Ok(Html(
        ListTemplate {
            site: &state.site,
            user_query: false,
            query: d,
            revision_links: rev_links,
            sbl_hits,
            verbose_links: verbose,
            receive_backlogged,
        }
        .render()
        .map_err(|e| anyhow!(e))?,
    ))
}

fn is_bot(ua: &str) -> bool {
    const CRAWLERS: [&str; 8] = [
        "IABot",
        "Googlebot",
        "Baiduspider",
        "ClaudeBot",
        "facebookexternalhit",
        "meta-externalagent",
        "myelfSearchBot",
        "Scrapy",
    ];
    CRAWLERS.iter().any(|c| ua.contains(c))
}

fn log_elapsed(elapsed: Duration, is_bot: bool) {
    if elapsed < Duration::from_secs(10) {
        if !is_bot {
            info!("gathering data: {:.2}s", elapsed.as_secs_f32());
        }
    } else {
        warn!("gathering data: {:.2}s", elapsed.as_secs_f32());
    }
}

#[instrument(name = "list", err(Debug), skip_all, fields(uri = uri.to_string(), referer, ua = ua))]
async fn list_by_user(
    params: Query<QueryParams>,
    State(state): State<AppState>,
    uri: Uri,
    ExtractReferer(referer): ExtractReferer,
    ExtractUserAgent(ua): ExtractUserAgent,
) -> Result<Html<String>, AppError> {
    if is_disguised_crawler(&referer, &ua, &uri, &state) {
        return empty_result(&state, "");
    }

    if !referer.is_empty() {
        tracing::Span::current().record("referer", &referer);
    }

    let u = params.q.as_str().replace('_', " ");
    let verbose = params
        .verbose
        .as_deref()
        .is_none_or(|v| v != "0" && !v.eq_ignore_ascii_case("false"));
    if u.is_empty() {
        return Err(AppError::BadRequest("The user name is empty."));
    }

    let time = std::time::Instant::now();
    let receive_backlogged = get_backlogged(&state).await?;
    let q = if let Ok(ip) = IpAddr::from_str(&u) {
        QueryBy::Ip(ip)
    } else if let Ok(inet) = IpInet::from_str(&u) {
        QueryBy::Subnet(inet.network())
    } else if let Some(id) = get_global_user_id(&u, &state.replica_pools).await? {
        QueryBy::User(id, &u)
    } else {
        return Err(AppError::BadRequest("Unknown user."));
    };
    let added_links = get_added_links(
        &q,
        1000,
        &state.pool,
        &state.replica_pools,
        &state.action_acs,
        state.nss_cache.clone(),
    )
    .await?;
    let rev_links = group_by_wiki(added_links);
    log_elapsed(time.elapsed(), is_bot(&ua));

    Ok(Html(
        ListTemplate {
            site: &state.site,
            user_query: true,
            query: &u,
            revision_links: rev_links,
            sbl_hits: Vec::new(),
            verbose_links: verbose,
            receive_backlogged,
        }
        .render()
        .map_err(|e| anyhow!(e))?,
    ))
}

fn empty_result(state: &AppState, q: &str) -> Result<Html<String>, AppError> {
    Ok(Html(
        ListTemplate {
            site: &state.site,
            user_query: false,
            query: q,
            revision_links: Vec::new(),
            sbl_hits: Vec::new(),
            verbose_links: false,
            receive_backlogged: None,
        }
        .render()
        .map_err(|e| anyhow!(e))?,
    ))
}

fn is_disguised_crawler(referer: &str, ua: &str, uri: &Uri, state: &AppState) -> bool {
    if referer.is_empty()
        && matches!(
            ua,
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.64 Safari/537.36 Edg/101.0.1210.47"
                | "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36"
                | "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/104.0.0.0 Safari/537.36"
                | "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"
                | "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36"
                | "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/605.1.15 (KHTML, like Gecko; compatible; FriendlyCrawler/1.0) Chrome/120.0.6099.216 Safari/605.1.15"
        )
    {
        return true;
    }
    if ua
        != "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"
    {
        return false;
    }
    let crawler_request = referer.parse::<Uri>().is_ok_and(|ref_uri| {
        ref_uri.host().is_some()
            && ref_uri.host()
                == state
                    .site
                    .parse::<Uri>()
                    .expect("site is a valid URI")
                    .host()
            && ref_uri.path_and_query() == uri.path_and_query()
    });
    crawler_request
}

fn group_by_wiki(added_links: db::QueryResult) -> Vec<(String, Vec<RevisionLinks>)> {
    let mut rev_links = Vec::new();
    for (group, iter) in &added_links
        .rev_links
        .into_iter()
        .chunk_by(|al| al.domain.clone())
    {
        rev_links.push((group.to_string(), iter.collect::<Vec<_>>()));
    }
    rev_links
}

async fn get_backlogged(state: &AppState) -> Result<Option<time::Duration>, AppError> {
    let event_times = get_last_event_times(&state.pool).await?;
    let receive_backlogged = Some(OffsetDateTime::now_utc() - event_times.received)
        .filter(|d| d > &Duration::from_secs(60));
    Ok(receive_backlogged)
}

async fn watch_sbls(sblc: SblCheck, api_client_service: ApiClientService) -> Result<()> {
    wikimedia_eventstreams::Builder::new()
        .user_agent(user_agent())
        .build::<PageChange>()
        .expect("should work")
        .map_err(anyhow::Error::from)
        .try_for_each(|ev| async {
            let data = ev.data;
            sblc.page_changed(&data, &api_client_service).await?;
            Ok(())
        })
        .await?;
    Ok(())
}

#[allow(clippy::too_many_lines)]
#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {
    init_logging();

    let version = option_env!("CI_COMMIT_TAG");
    if let Some(version) = version {
        tracing::info!("Starting {}...", version);
    } else {
        tracing::info!("Starting...");
    }

    let config = load_config();
    let conn_opt = config
        .local_db_url
        .parse::<MySqlConnectOptions>()?
        .disable_statement_logging();
    let pool = MySqlPoolOptions::new()
        .max_connections(18)
        .min_connections(0)
        .idle_timeout(Duration::from_secs(3))
        .acquire_timeout(Duration::from_secs(300))
        .connect_with(conn_opt)
        .await?;
    let replica_pools = replica_pools::Builder::new()
        .with_custom_connection(config.replicas.urls)
        .with_slow_statement_threshold(Duration::from_secs(10))
        .build(&config.replicas.username, &config.replicas.password)
        .await?;

    let action_acs = new_burstable_action_api_client_service(user_agent())?;
    let all = replica_pools.all_wikis();
    let i = all.iter().map(|w| w.url.as_str());
    let nss_cache = NamespacesCache::new();
    let sblc;
    if std::env::var("TOOL_DATA_DIR").is_ok() {
        info!("Caching namespaces for all wikis...");
        let time = std::time::Instant::now();
        nss_cache.ensure_namespaces_cached(&action_acs, i).await?;
        info!(
            "Caching namespaces took {:.2} seconds.",
            time.elapsed().as_secs_f32()
        );
        info!("Caching SBLs...");
        let time = std::time::Instant::now();
        sblc = sbl::SblCheck::new(&action_acs).await?;
        info!(
            "Caching SBLs took {:.2} seconds.",
            time.elapsed().as_secs_f32()
        );
    } else {
        sblc = sbl::SblCheck::empty();
        let sbl_clone = sblc.clone();
        task::spawn(async move {
            info!("Caching SBLs in background...");
            let time = std::time::Instant::now();
            sbl_clone
                .update_all(
                    &new_sbl_api_client_service(user_agent()).expect("create sbl api client"),
                )
                .await
                .expect("update sbl");
            info!(
                "Caching SBLs took {:.2} seconds.",
                time.elapsed().as_secs_f32()
            );
        });
    }
    let state = AppState {
        pool,
        replica_pools,
        action_acs,
        nss_cache,
        sblc,
        site: config.site,
    };
    let sbl_clone = state.sblc.clone();
    let acs_clone = state.action_acs.clone();
    task::spawn(async move {
        loop {
            match watch_sbls(sbl_clone.clone(), acs_clone.clone()).await {
                Ok(()) => {
                    error!("SBLs watcher stopped");
                }
                Err(e) => {
                    error!("Error watching SBLs: {e}");
                }
            }
        }
    });

    let index_handler = move || async {
        let index_html: &'static str = include_str!("index.html");
        Ok::<_, AppError>(Html(index_html))
    };
    let app = Router::new()
        .route("/", routing::get(index_handler))
        .route("/healthz", routing::get(health_check))
        .route("/by-domain", routing::get(list_by_domain))
        .route("/by-user", routing::get(list_by_user))
        .with_state(state)
        .layer(TraceLayer::new_for_http());

    let listener = if cfg!(debug_assertions) {
        tokio::net::TcpListener::bind("127.0.0.1:8000").await?
    } else {
        let port = std::env::var("PORT")
            .unwrap_or_else(|_| "8000".to_string())
            .parse()?;
        let addr = SocketAddr::new(Ipv4Addr::UNSPECIFIED.into(), port);
        tokio::net::TcpListener::bind(addr).await?
    };

    axum::serve(listener, app).await?;
    Ok(())
}
