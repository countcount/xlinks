use std::convert::Infallible;

use axum::{
    extract::FromRequestParts,
    http::{
        HeaderName,
        header::{REFERER, USER_AGENT},
        request::Parts,
    },
};

#[derive(Debug)]
#[allow(clippy::module_name_repetitions)]
pub struct ExtractUserAgent(pub String);

#[derive(Debug)]
#[allow(clippy::module_name_repetitions)]
pub struct ExtractReferer(pub String);

fn extract_header(parts: &Parts, header: &HeaderName) -> String {
    parts
        .headers
        .get(header)
        .map_or("", |v| v.to_str().unwrap_or(""))
        .to_string()
}

impl<S> FromRequestParts<S> for ExtractUserAgent
where
    S: Send + Sync,
{
    type Rejection = Infallible;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Infallible> {
        Ok(Self(extract_header(parts, &USER_AGENT)))
    }
}

impl<S> FromRequestParts<S> for ExtractReferer
where
    S: Send + Sync,
{
    type Rejection = Infallible;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Infallible> {
        Ok(Self(extract_header(parts, &REFERER)))
    }
}
