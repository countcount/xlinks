use std::fmt::{Display, Formatter};
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};

use anyhow::Result;
use cidr::IpCidr;
use futures_util::stream::FuturesUnordered;
use futures_util::{StreamExt, TryStreamExt, try_join};
use shared::api_client::ApiClientService;
use sqlx::types::time::OffsetDateTime;
use sqlx::{MySql, Pool, QueryBuilder, query_as, query_scalar};
use tokio::sync::RwLock;

use replica_pools::{ReplicaPools, SHARD_COUNT};
use shared::{indexify, unindexify};

use crate::namespaces::NamespacesCache;

#[derive(Debug, PartialEq, Eq)]
pub enum User {
    IPv4(Ipv4Addr),
    IPv6(Ipv6Addr),
    Global(String),
    Other(String),
}

impl Display for User {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IPv4(ip) => write!(f, "{ip}"),
            Self::IPv6(ip) => write!(f, "{}", ip.to_string().to_ascii_uppercase()),
            Self::Global(ca) => write!(f, "{ca}"),
            Self::Other(ut) => write!(f, "{ut}"),
        }
    }
}

pub enum QueryBy<'a> {
    Domain(&'a str),
    User(u32, &'a str),
    Ip(IpAddr),
    Subnet(IpCidr),
}

pub struct QueryResult {
    pub rev_links: Vec<RevisionLinks>,
    #[allow(dead_code)] // TODO: use
    pub limit_reached: bool,
    #[allow(dead_code)] // TODO: use
    pub some_oversighted: bool,
}

#[derive(Debug, PartialEq, Eq)]
#[allow(clippy::struct_excessive_bools)]
pub struct RevisionLinks {
    pub domain: String,
    pub revision: u32,
    pub timestamp: OffsetDateTime,
    pub user: User,
    pub bot: bool,
    pub minor_edit: bool,
    pub new_page: bool,
    pub reverted: bool,
    pub page_deleted: bool,
    #[allow(dead_code)] // TODO: use
    pub allowlisted: bool,
    #[allow(dead_code)] // TODO: use
    pub host_added_count: u32,
    #[allow(dead_code)] // TODO: use
    pub host_removed_count: u32,
    pub page_title: String,
    pub urls: Vec<AddedUrl>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct AddedUrl {
    pub url: String,
    pub domain: String,
    pub still_present: bool,
}

pub async fn get_added_links(
    query: &QueryBy<'_>,
    limit: u32,
    pool: &Pool<MySql>,
    replica_pools: &ReplicaPools,
    action_acs: &ApiClientService,
    nss_cache: NamespacesCache,
) -> Result<QueryResult> {
    let als = get_als_from_xlinks(pool, query, limit).await?;
    let limit_reached = u32::try_from(als.len()).expect("len() fits in u32") == limit;
    let (rev_links, some_oversighted) =
        annotate_added_links(query, replica_pools, als, nss_cache, action_acs).await?;
    Ok(QueryResult {
        rev_links,
        limit_reached,
        some_oversighted,
    })
}

pub async fn get_global_user_id(name: &str, replica_pools: &ReplicaPools) -> Result<Option<u32>> {
    let mut conn = replica_pools.acquire_for_centralauth().await?;
    Ok(query_scalar::<_, i32>(
        "
        SELECT gu_id
        FROM centralauth_p.globaluser
        WHERE gu_name = ?
        ",
    )
    .bind(name.as_bytes())
    .fetch_optional(&mut *conn)
    .await?
    .map(|id| u32::try_from(id).expect("global user id should fit into u32")))
}

pub struct EventTimes {
    pub received: OffsetDateTime,
    #[allow(dead_code)] // TODO: use
    pub processed: OffsetDateTime,
}

pub async fn get_last_event_times(pool: &Pool<MySql>) -> Result<EventTimes> {
    let mut conn = pool.acquire().await?;
    let times = query_as::<_, (String, OffsetDateTime)>(
        "
        SELECT CONVERT(lp_type USING utf8mb4), lp_time
        FROM lastprocessed
        ",
    )
    .fetch_all(&mut *conn)
    .await?;

    let mut received = None;
    let mut processed = None;
    assert_eq!(times.len(), 2, "last_processed should have 2 rows");
    for (lp_type, lp_time) in times {
        match lp_type.as_str() {
            "received" => received = Some(lp_time),
            "processed" => processed = Some(lp_time),
            _ => panic!("unexpected lp_type: {lp_type}"),
        }
    }
    Ok(EventTimes {
        received: received.expect("received time should be present"),
        processed: processed.expect("processed time should be present"),
    })
}

#[derive(Debug, sqlx::FromRow)]
#[allow(clippy::struct_excessive_bools)]
struct DbAddedLink {
    db_domain: String,
    db_name: String,
    rev_revision: u32,
    rev_page: u32,
    rev_timestamp: OffsetDateTime,
    rev_user_centralauth: Option<u32>,
    rev_ipv6: Option<Vec<u8>>,
    rev_ipv4: Option<u32>,
    rev_bot: bool,
    rev_minor_edit: bool,
    rev_new_page: bool,
    rev_reverted: bool,
    rev_page_deleted: bool,
    sch_scheme: String,
    hst_host: String,
    al_path: Option<String>,
    pof_rest: Option<String>,
    hst_allowlisted: bool,
    hst_added_count: u32,
    hst_removed_count: u32,
    ut_text: Option<String>,
}

async fn annotate_added_links(
    query: &QueryBy<'_>,
    replica_pools: &ReplicaPools,
    mut als: Vec<DbAddedLink>,
    nss_cache: NamespacesCache,
    action_acs: &ApiClientService,
) -> Result<(Vec<RevisionLinks>, bool), anyhow::Error> {
    // can't sort in db because we query for the latest links there
    als.sort_unstable_by(|x, y| {
        x.db_domain
            .cmp(&y.db_domain)
            .then_with(|| x.rev_timestamp.cmp(&y.rev_timestamp).reverse())
            .then_with(|| x.rev_revision.cmp(&y.rev_revision))
    });

    // filter out wikis that have no entry in the meta_p.wiki table (yet)
    als.retain(|al| {
        !replica_pools
            .get_shard(&al.db_name)
            .is_err_and(|e| matches!(e, replica_pools::Error::WikiNotFound(_)))
    });

    let (user_names, titles, ()) = try_join!(
        get_global_user_names(query, replica_pools, &als),
        get_replica_data(replica_pools, &als),
        nss_cache.ensure_namespaces_cached(action_acs, als.iter().map(|al| al.db_domain.as_str()))
    )?;

    assert_eq!(als.len(), titles.len());
    assert_eq!(als.len(), user_names.len());
    let mut some_oversighted = false;
    let mut res = Vec::with_capacity(als.len());
    let mut last_revision: Option<RevisionLinks> = None;
    for ((al, rd), ca) in als
        .into_iter()
        .zip(titles.into_iter())
        .zip(user_names.into_iter())
    {
        match last_revision.as_mut() {
            Some(last_revision_ex) => {
                if last_revision_ex.domain != al.db_domain
                    || last_revision_ex.revision != al.rev_revision
                {
                    if let Some(rl) = assemble_revision_links(al, rd, ca, &nss_cache).await {
                        res.push(std::mem::replace(last_revision_ex, rl));
                    } else {
                        res.push(last_revision.take().expect("last_revision cannot be None"));
                        some_oversighted = true;
                    }
                } else {
                    last_revision_ex.urls.push(assemble_added_url(&al, &rd));
                }
            }
            None => {
                if let Some(rl) = assemble_revision_links(al, rd, ca, &nss_cache).await {
                    last_revision = Some(rl);
                } else {
                    some_oversighted = true;
                }
            }
        }
    }
    if let Some(last_revision) = last_revision {
        res.push(last_revision);
    }
    for rev in &mut res {
        rev.urls.sort_unstable_by(|x, y| x.url.cmp(&y.url));
    }
    Ok((res, some_oversighted))
}

fn assemble_added_url(al: &DbAddedLink, rd: &ReplicaData) -> AddedUrl {
    AddedUrl {
        domain: assemble_domain(al),
        url: assemble_url(al),
        still_present: rd.still_present,
    }
}

fn assemble_domain(al: &DbAddedLink) -> String {
    let res = unindexify(&al.hst_host);
    if res.to_ascii_lowercase().starts_with("www.") {
        res[4..].to_string()
    } else {
        res
    }
}

fn assemble_url(al: &DbAddedLink) -> String {
    format!(
        "{}{}{}{}",
        al.sch_scheme,
        unindexify(&al.hst_host),
        al.al_path.as_deref().unwrap_or_default(),
        al.pof_rest.as_deref().unwrap_or_default()
    )
}

fn assemble_el_domain_index(al: &DbAddedLink) -> String {
    format!("{}{}", al.sch_scheme, &al.hst_host)
}

fn assemble_el_path(al: &DbAddedLink) -> String {
    let res = format!(
        "{}{}",
        al.al_path.as_deref().unwrap_or_default(),
        al.pof_rest.as_deref().unwrap_or_default()
    );
    if res.is_empty() { "/".to_string() } else { res }
}

async fn assemble_revision_links(
    al: DbAddedLink,
    rd: ReplicaData,
    ca: Option<String>,
    nss_cache: &NamespacesCache,
) -> Option<RevisionLinks> {
    let url = assemble_added_url(&al, &rd);
    let page_title = rd.title.or(rd.deleted_title);
    let page_title = if let Some(title) = page_title {
        if let Some(namespace) = rd.namespace {
            let mut page_title = nss_cache.get(&al.db_domain, namespace).await;
            if !page_title.is_empty() {
                page_title.push(':');
            }
            page_title.push_str(&title);
            page_title
        } else {
            title
        }
    } else {
        // oversighted
        return None;
    };

    Some(RevisionLinks {
        domain: al.db_domain,
        revision: al.rev_revision,
        timestamp: al.rev_timestamp,
        user: if let Some(ipv4) = al.rev_ipv4 {
            User::IPv4(Ipv4Addr::from(ipv4))
        } else if let Some(ipv6) = al.rev_ipv6 {
            let ipv6 = Ipv6Addr::from(
                <[u8; 16]>::try_from(ipv6).expect("ip address must be 16 bytes long"),
            );
            User::IPv6(ipv6)
        } else if let Some(ca) = ca {
            User::Global(ca)
        } else if let Some(ut_text) = al.ut_text {
            User::Other(ut_text)
        } else {
            User::Other("(unknown)".to_string())
        },
        bot: al.rev_bot,
        minor_edit: al.rev_minor_edit,
        new_page: al.rev_new_page,
        reverted: al.rev_reverted,
        page_deleted: al.rev_page_deleted,
        allowlisted: al.hst_allowlisted,
        host_added_count: al.hst_added_count,
        host_removed_count: al.hst_removed_count,
        page_title,
        urls: vec![url],
    })
}

#[derive(sqlx::FromRow)]
struct ReplicaData {
    no: i32,
    title: Option<String>,
    deleted_title: Option<String>,
    namespace: Option<i32>,
    still_present: bool,
}

#[allow(clippy::too_many_lines)]
async fn get_replica_data(
    replica_pools: &ReplicaPools,
    als: &[DbAddedLink],
) -> Result<Vec<ReplicaData>> {
    let mut qbs: Vec<Option<QueryBuilder<MySql>>> = Vec::with_capacity(SHARD_COUNT);
    qbs.resize_with(SHARD_COUNT, || None);
    let mut curr_db_qb: Option<(&str, &mut QueryBuilder<MySql>)> = None;
    let mut first = false;
    for (i, al) in als.iter().enumerate() {
        let db_name = al.db_name.as_str();
        if curr_db_qb.as_ref().is_none_or(|(db, _)| *db != db_name) {
            first = true;
            if let Some((old_db_name, old_qb)) = curr_db_qb.as_mut() {
                // finalize current select
                old_qb.push(format!(
                    "
                        ) q
                        LEFT JOIN {old_db_name}_p.page ON page.page_id = q.page
                        LEFT JOIN {old_db_name}_p.archive ON ar_rev_id = q.rev
                        LEFT JOIN {old_db_name}_p.externallinks ON el_from = q.page
                        AND el_to_domain_index = q.domain AND el_to_path = q.path
                        WHERE no IS NOT NULL
                    "
                ));
            }
            if let Some(qb) = qbs[usize::from(replica_pools.get_shard(db_name)?)].as_mut() {
                // union with previous select
                qb.push(
                    "
                        UNION ALL
                    ",
                );
            }
            let qb = qbs[usize::from(replica_pools.get_shard(db_name)?)]
                .get_or_insert_with(|| QueryBuilder::<MySql>::new(""));
            qb.push(
                "SELECT DISTINCT no, CONVERT(page_title USING utf8) as title, CONVERT(ar_title USING utf8) as deleted_title, page_namespace as namespace, el_id IS NOT NULL as still_present FROM (SELECT NULL AS no, NULL AS page, NULL as rev, NULL as domain, NULL as path UNION ALL VALUES ");
            curr_db_qb = Some((db_name, qb));
        }
        let curr_qb = &mut *curr_db_qb
            .as_mut()
            .expect("current query should be Some(_) here")
            .1;
        if first {
            first = false;
        } else {
            curr_qb.push(", ");
        }
        // MariaDB 10.6.14 has bugs with bind parameters in VALUES clauses
        let use_bind = false;
        curr_qb.push("\n(");
        if use_bind {
            curr_qb.push_bind(i32::try_from(i).expect("id should fit into i32"));
            curr_qb.push(",");
            curr_qb.push_bind(al.rev_page);
            curr_qb.push(",");
            curr_qb.push_bind(al.rev_page_deleted.then_some(al.rev_revision));
            curr_qb.push(", ");
            curr_qb.push_bind(assemble_el_domain_index(al));
            curr_qb.push(", ");
            curr_qb.push_bind(assemble_el_path(al));
            curr_qb.push(")");
        } else {
            curr_qb.push(i32::try_from(i).expect("id should fit into i32"));
            curr_qb.push(",");
            curr_qb.push(al.rev_page);
            curr_qb.push(",");
            curr_qb.push(if al.rev_page_deleted {
                al.rev_revision.to_string()
            } else {
                "NULL".to_string()
            });
            curr_qb.push(", '");
            curr_qb.push(escape(&assemble_el_domain_index(al)));
            curr_qb.push("', '");
            curr_qb.push(escape(&assemble_el_path(al)));
            curr_qb.push("')");
        }
    }
    if let Some((old_db_name, old_qb)) = curr_db_qb {
        // finalize last select
        old_qb.push(format!(
            "
                ) q
                LEFT JOIN {old_db_name}_p.page ON page.page_id = q.page
                LEFT JOIN {old_db_name}_p.archive ON ar_rev_id = q.rev
                LEFT JOIN {old_db_name}_p.externallinks ON el_from = q.page
                AND el_to_domain_index = q.domain AND el_to_path = q.path
                WHERE no IS NOT NULL
            "
        ));
    }

    let rds = RwLock::new(Vec::with_capacity(als.len()));
    let rds_ref = &rds;
    let futs = FuturesUnordered::new();
    for (shard_no, qb) in qbs.iter_mut().enumerate() {
        let Some(qb) = qb else {
            continue;
        };
        futs.push(async move {
            let mut conn = replica_pools
                .acquire_for_shard(shard_no.try_into()?)
                .await?;
            qb.build_query_as::<ReplicaData>()
                .fetch(&mut *conn)
                .try_for_each(|rd| async {
                    rds_ref.write().await.push(rd);
                    Ok(())
                })
                .await?;
            Result::<()>::Ok(())
        });
    }
    futs.try_for_each(|()| async { Ok(()) }).await?;
    rds.write().await.sort_by_key(|rd| rd.no);
    Ok(rds.into_inner())
}

fn escape(s: &str) -> String {
    s.replace('\\', r"\\").replace('\'', "''")
}

async fn get_global_user_names(
    query: &QueryBy<'_>,
    replica_pools: &ReplicaPools,
    als: &[DbAddedLink],
) -> Result<Vec<Option<String>>> {
    match query {
        QueryBy::Ip(_) | QueryBy::Subnet(_) => return Ok(vec![None; als.len()]),
        QueryBy::User(_, name) => return Ok(vec![Some((*name).to_string()); als.len()]),
        QueryBy::Domain(_) => {}
    }
    let cas = if als.iter().any(|al| al.rev_user_centralauth.is_some()) {
        let mut qb = QueryBuilder::<MySql>::new("WITH q(no, id) AS (VALUES ");
        let mut first = true;
        for (c, centralauth) in als.iter().map(|al| al.rev_user_centralauth).enumerate() {
            if first {
                first = false;
            } else {
                qb.push(",");
            }
            qb.push("(");
            qb.push(u32::try_from(c).expect("id should fit into u32"));
            qb.push(",");
            qb.push(centralauth.map_or("NULL".to_string(), |ca| ca.to_string()));
            qb.push(")");
        }
        qb.push(
            "
            ) SELECT CONVERT(centralauth_p.globaluser.gu_name USING utf8)
            FROM q
                     LEFT JOIN centralauth_p.globaluser ON gu_id = q.id
            ORDER BY q.no ASC;
        ",
        );
        let mut conn = replica_pools.acquire_for_centralauth().await?;
        qb.build_query_as::<(Option<String>,)>()
            .fetch(&mut *conn)
            .map(|r| r.map(|(o,)| o))
            .try_collect()
            .await?
    } else {
        vec![None; als.len()]
    };
    Ok(cas)
}

async fn get_als_from_xlinks(
    pool: &Pool<MySql>,
    qc: &QueryBy<'_>,
    limit: u32,
) -> Result<Vec<DbAddedLink>> {
    let mut qb = QueryBuilder::new(
        "
        SELECT CONVERT(db_domain USING utf8mb4) AS db_domain,
               CONVERT(db_name USING utf8mb4) AS db_name,
               rev_revision,
               rev_page,
               rev_timestamp,
               rev_user_centralauth,
               rev_ipv6,
               rev_ipv4,
               rev_bot,
               rev_minor_edit,
               rev_new_page,
               rev_reverted,
               rev_page_deleted,
               CONVERT(sch_scheme USING utf8mb4) AS sch_scheme,
               CONVERT(hst_host USING utf8mb4) AS hst_host,
               CONVERT(al_path USING utf8mb4) AS al_path,
               CONVERT(pof_rest USING utf8mb4) AS pof_rest,
               hst_allowlisted,
               hst_added_count,
               hst_removed_count,
               CONVERT(ut_text USING utf8mb4) AS ut_text
        FROM addedlink
                 INNER JOIN host ON al_host = hst_id
                 INNER JOIN revision ON al_rev = rev_id
                 INNER JOIN db ON rev_db = db_id
                 INNER JOIN scheme ON al_scheme = sch_id
                 LEFT JOIN pathoverflow ON al_path_overflow = pof_id
                 LEFT JOIN usertext ON rev_user_text = ut_id
        ",
    );
    let bind_ip1: Option<[u8; 16]>;
    let bind_ip2: Option<[u8; 16]>;
    match qc {
        QueryBy::Domain(domain) => {
            let mut domain = indexify(domain);
            domain.push('%');
            qb.push("WHERE hst_host LIKE ");
            qb.push_bind(domain);
        }
        QueryBy::User(ca, _) => {
            qb.push("WHERE rev_user_centralauth = ");
            qb.push_bind(ca);
        }
        QueryBy::Ip(ip) => match ip {
            IpAddr::V4(ipv4) => {
                qb.push("WHERE rev_ipv4 = ");
                let ipv4 = u32::from(*ipv4);
                qb.push_bind(ipv4);
            }
            IpAddr::V6(ipv6) => {
                qb.push("WHERE rev_ipv6 = ");
                bind_ip1 = ipv6.octets().into();
                qb.push_bind(bind_ip1.as_ref().map(|b| b.as_ref()));
            }
        },
        QueryBy::Subnet(cidr) => match cidr {
            IpCidr::V4(cidr) => {
                let ip1: u32 = cidr.first().address().into();
                let ip2: u32 = cidr.last().address().into();
                qb.push("WHERE rev_ipv4 BETWEEN ");
                qb.push_bind(ip1);
                qb.push(" AND ");
                qb.push_bind(ip2);
            }
            IpCidr::V6(cidr) => {
                bind_ip1 = cidr.first().address().octets().into();
                bind_ip2 = cidr.last().address().octets().into();
                qb.push("WHERE rev_ipv6 BETWEEN ");
                qb.push_bind(bind_ip1.as_ref().map(|b| b.as_ref()));
                qb.push(" AND ");
                qb.push_bind(bind_ip2.as_ref().map(|b| b.as_ref()));
            }
        },
    }
    qb.push(
        "
            ORDER BY rev_id DESC
            LIMIT
        ",
    );
    qb.push_bind(limit);
    let als = qb.build_query_as::<DbAddedLink>().fetch_all(pool).await?;
    Ok(als)
}
