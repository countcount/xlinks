// askama filters have to return a `Result` type
#![allow(clippy::unnecessary_wraps)]

use percent_encoding::{AsciiSet, NON_ALPHANUMERIC, utf8_percent_encode};
use std::fmt::{Display, Write};

use time::{Duration, OffsetDateTime, format_description};

pub fn mediawiki_date(d: &OffsetDateTime) -> askama::Result<String> {
    let format = format_description::parse("[year]-[month]-[day] [hour]:[minute]")
        .expect("date format should be valid");
    Ok(d.format(&format).map_err(|_| std::fmt::Error)?)
}

pub fn human_readable_duration(d: &Duration) -> askama::Result<String> {
    let s = d.whole_seconds();
    if s == 0 {
        return Ok("0 seconds".to_string());
    }

    let time_units = [
        (s / 86400, "day"),
        ((s % 86400) / 3600, "hour"),
        ((s % 3600) / 60, "minute"),
        (s % 60, "second"),
    ];

    let mut res = String::new();
    for (v, u) in time_units {
        if v > 0 {
            if !res.is_empty() {
                res.push_str(", ");
            }
            write!(&mut res, "{} {}{}", v, u, if v == 1 { "" } else { "s" }).unwrap();
        }
    }
    Ok(res)
}

const URLENCODE_QUERY_SET: &AsciiSet = &NON_ALPHANUMERIC
    .remove(b'_')
    .remove(b'.')
    .remove(b'-')
    .remove(b'~')
    .remove(b'/')
    .remove(b' ');

pub fn queryencode<T: Display>(s: T) -> askama::Result<String> {
    let s = s.to_string();
    let mut res = utf8_percent_encode(&s, URLENCODE_QUERY_SET).to_string();
    replace_ascii(&mut res, b' ', b'+');
    Ok(res)
}

pub fn underscore_to_space<T: Display>(s: T) -> askama::Result<String> {
    let mut res = s.to_string();
    replace_ascii(&mut res, b'_', b' ');
    Ok(res)
}

pub fn space_to_underscore<T: Display>(s: T) -> askama::Result<String> {
    let mut res = s.to_string();
    replace_ascii(&mut res, b' ', b'_');
    Ok(res)
}

fn replace_ascii(s: &mut str, from: u8, to: u8) {
    assert!(from.is_ascii());
    assert!(to.is_ascii());
    unsafe {
        for b in s.as_bytes_mut() {
            if *b == from {
                *b = to;
            }
        }
    }
}
