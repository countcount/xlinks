use std::{
    collections::{BTreeMap, HashMap, HashSet},
    sync::Arc,
};

use anyhow::{Error, Result};
use futures_util::{TryStreamExt, stream::FuturesUnordered};
use shared::api_client::{ApiClientService, get_with_retry};
use tokio::sync::RwLock;

#[derive(Clone)]
#[allow(clippy::module_name_repetitions)]
pub struct NamespacesCache {
    all_namespaces: Arc<RwLock<HashMap<String, Namespaces>>>,
}

impl NamespacesCache {
    pub fn new() -> Self {
        Self {
            all_namespaces: Arc::new(RwLock::new(HashMap::new())),
        }
    }

    pub async fn get(&self, domain: &str, no: i32) -> String {
        self.all_namespaces
            .read()
            .await
            .get(domain)
            .and_then(|nss| nss.get(no))
            .map_or_else(|| no.to_string(), |s| s.to_string())
    }

    pub async fn ensure_namespaces_cached<'a, I>(
        &self,
        action_acs: &ApiClientService,
        domains: I,
    ) -> Result<()>
    where
        I: IntoIterator<Item = &'a str> + Send,
    {
        let mut domains = domains.into_iter().collect::<HashSet<_>>();
        {
            let all_namespaces = self.all_namespaces.read().await;
            domains.retain(|domain| !all_namespaces.contains_key(*domain));
        }
        if domains.is_empty() {
            return Ok(());
        }
        let mut futs = FuturesUnordered::new();
        for domain in domains {
            let mut domain = domain;
            if domain.starts_with("https://") {
                domain = &domain["https://".len()..];
            }
            let action_acs = action_acs.clone();
            futs.push(async move {
                Result::<_, Error>::Ok((
                    domain.to_string(),
                    get_namespaces(action_acs, domain).await?,
                ))
            });
        }
        while let Some((domain, nss)) = futs.try_next().await? {
            self.all_namespaces.write().await.insert(domain, nss);
        }
        Ok(())
    }
}

struct Namespaces {
    namespaces: Box<[(i32, String)]>,
}

impl Namespaces {
    fn get(&self, id: i32) -> Option<&str> {
        self.namespaces
            .binary_search_by_key(&id, |(id, _)| *id)
            .ok()
            .map(|i| self.namespaces[i].1.as_str())
    }
}

async fn get_namespaces(acs: ApiClientService, domain: &str) -> Result<Namespaces> {
    #[derive(serde::Deserialize)]
    struct QueryResult {
        query: Query,
    }
    #[derive(serde::Deserialize)]
    struct Query {
        namespaces: BTreeMap<i32, Namespace>,
    }
    #[derive(serde::Deserialize)]
    struct Namespace {
        #[serde(rename = "*")]
        name: String,
    }

    let url = format!(
        "https://{domain}/w/api.php?action=query&meta=siteinfo&siprop=namespaces&format=json"
    );
    let text = get_with_retry(&url, acs).await?;
    let res = serde_json::from_str::<QueryResult>(&text)?;
    let nss = res
        .query
        .namespaces
        .into_iter()
        .map(|(id, ns)| (id, ns.name))
        .collect::<Vec<_>>()
        .into_boxed_slice();
    Ok(Namespaces { namespaces: nss })
}

#[cfg(test)]
mod test {
    use anyhow::Result;
    use shared::api_client::new_action_api_client_service;

    use crate::util::user_agent;

    #[tokio::test]
    async fn test_get_dewiki_namespaces() -> Result<()> {
        let x = super::get_namespaces(
            new_action_api_client_service(user_agent())?,
            "de.wikipedia.org",
        )
        .await?;
        assert_eq!(x.get(0), Some(""));
        assert_eq!(x.get(1), Some("Diskussion"));
        Ok(())
    }
}
