#!/usr/bin/env bash
set -euo pipefail

source ../deploy-common.sh

release_and_deploy spamcheck spamcheck spamcheck bin/spamcheck "webservice restart"
