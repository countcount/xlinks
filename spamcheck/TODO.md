# TODO

## Immediate
* P1 - BUG: /by-user?q=StephenMacky1
* P1 - version in user agent (xlinks too) and in footer
* P1 - user search: extra form page
* P1 - backlink to form (breadcrumbs)
* P1 - empty result set message
* P1 - behavior if some or all domains are allowlisted
  * don't show allowlisted domains with warning (all hosts allowlisted)
  * show warning im some hosts allowlisted including which hosts
* P1 - Grouping by date/wiki/user
* P1 - invalid characters error page, link to check domain only
* P1 - no namespace displayed for deleted pages


* P2 - replag: warn and show partial results
* P2 - page with allowlisted hosts
* P2 - filter: namespace, wiki, ...
* P2 - docs
* P2 - links for docs, bug report, talk page similar to xtools
* P2 - phabricator link  https://phabricator.wikimedia.org/project/profile/7062/
* P2 summary:
  * hosts of external links (added/removed/still present count, allowlisted, SBLed)
  * users/ips adding
* P2 force-unallowlist, e.g. sites.google.com
* P2 multiple users/domains at once
* P2 - sort by time
* P2 - (needs global search) Show hint if there are more pages with links that the additions don't account for
* P2 - global search link

* P3 - add health tests
* P3 - gls
* P3 - whitelist rollbackers/patroller/admin on user talk pages (and/or with twinkle edit tag), common in enwp
* P3 - allow http:// and https:// urls in search -> strip to domain
* P3 - multi-user/multi-domain search
* P3 - make icon for external links red if not present
* P3 - manually allowlist all wikimedia wikis
* P3 - have partitioned tables storing all addition/removal for the last 2-3 months
* P3 anchors for grouped-by headings and Results

* P? - warning if result contains allowlisted hosts when domain searching, should only happen if limit is reached
* P? - warning if limit is reached
* P? - display results on same page as form
* P? - path checking

## Unsorted
* SBL
* Report to meta:Talk:SBL
* ? option to include domains added with the same revision
* User: +CA link (?)
* show "all times in utc" somewhere
* date format
* add error logging for all returned errors
* multi-domain, multi-user/ip search
* multi-domain: allow wildcards at beginning, and with secret flag for everything (?)
* ? show "still present" if link is still present
* better layout on mobile
* footer template
* optics (better font)
* import LiWa3 data
* ? form on results page
* proper performance logging
* ? edit summary
* try to use buildservice, again.
* show allowlisted

## Later
* global data for affected hosts: allowlisted, added/removed stats, later: SBLed as of now
* ? individual URLs: check SBLed as of now
* ? user/ip blocked/globally (b)locked?

## Options
* list all urls added in each edit, not just matching ones
* group by: date->wiki, wiki->data, user->date->wiki, user->wiki->date
* hidden: limit
* hidden: wildcards
* verbose
