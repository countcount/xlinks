SELECT
    db_domain,
    db_name,
    rev_revision,
    rev_page,
    rev_timestamp,
    rev_user_centralauth,
    rev_ip,
    rev_bot,
    rev_minor_edit,
    rev_new_page,
    rev_reverted,
    rev_page_deleted,
    sch_scheme,
    hst_host,
    al_path,
    pof_rest,
    hst_allowlisted,
    hst_added_count,
    hst_removed_count
FROM
    addedlink
    INNER JOIN host ON al_host = hst_id
    INNER JOIN revision ON al_db = rev_db
    AND al_rev = rev_revision
    INNER JOIN db ON rev_db = db_id
    INNER JOIN scheme ON al_scheme = sch_id
    LEFT JOIN pathoverflow ON al_path_overflow = pof_id
    LEFT JOIN usertext ON rev_user_text = ut_id
WHERE
    hst_host LIKE 'uk.co.bbc.%'
ORDER BY
    db_domain,
    rev_timestamp DESC
LIMIT
    1000;

--- analyze big tables
ANALYZE TABLE host;

ANALYZE TABLE revision;

ANALYZE TABLE addedlink;

ANALYZE TABLE pathoverflow;

-- table sizes
SELECT
    table_name,
    ROUND(data_length / 1024 / 1024, 2) AS data,
    ROUND(index_length / 1024 / 1024, 2) as indexes,
    ROUND((data_length + index_length) / 1024 / 1024, 2) AS total
FROM
    information_schema.TABLES
where
    table_schema = 's55543__xlinks_p'
    and table_name in ('host', 'revision', 'addedlink', 'pathoverflow')
order by
    data + indexes;

SELECT
    ROUND(sum(data_length / 1024 / 1024), 2) AS data,
    ROUND(SUM(index_length / 1024 / 1024), 2) as indexes,
    ROUND(
        SUM((data_length + index_length) / 1024 / 1024),
        2
    ) AS total
FROM
    information_schema.TABLES
where
    table_schema = 's55543__xlinks_p'
    and table_name in ('host', 'revision', 'addedlink', 'pathoverflow');

-- multi - adds, last day, enwiki
select
    hst_host,
    count(distinct rev_page) as cnt,
    sum(rev_reverted = 1) as rev,
    sum(rev_reverted = 0) as nrev,
    hst_added_count,
    hst_removed_count
FROM
    addedlink al
    JOIN host h ON al.al_host = h.hst_id
    JOIN revision r ON al.al_rev = r.rev_id
    JOIN db d on r.rev_db = d.db_id
WHERE
    r.rev_timestamp >= NOW() - INTERVAL 1 DAY
    AND hst_added_count < 100
    AND db_name = 'enwiki'
group by
    hst_host
having
    rev > 0
    and nrev > 0
order by
    cnt;

-- multi - adds, last day, crosswiki
select
    hst_host,
    count(distinct rev_page) as cnt,
    sum(rev_reverted = 1) as rev,
    sum(rev_reverted = 0) as nrev,
    hst_added_count,
    hst_removed_count
FROM
    addedlink al
    JOIN host h ON al.al_host = h.hst_id
    JOIN revision r ON al.al_rev = r.rev_id
    JOIN db d on r.rev_db = d.db_id
WHERE
    r.rev_timestamp >= NOW() - INTERVAL 1 DAY
    AND hst_added_count < 100
group by
    hst_host
having
    rev > 0
    and nrev > 0
    and count(distinct rev_db) > 1
order by
    cnt;
