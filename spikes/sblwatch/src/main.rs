use std::{
    cell::RefCell,
    collections::{HashMap, HashSet, VecDeque},
    hash::Hash,
};

use futures::StreamExt;
use sbl::{Hit, SblCheck};
use shared::api_client;
use tracing::{Level, info};
use tracing_subscriber::{filter, prelude::*};
use url::Url;
use wikimedia_events::{
    Any, page_change::PageChange, page_links_change::PageLinksChange,
    revision_create::RevisionCreate,
};
use wikimedia_eventstreams::MultipleStreams;

const PATTERN_ALLOWLIST: &str = r"health.*
__
.*\.ru\b
\byoutube\.com\b
\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}
(?<=//)\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b
\bfacebook\.com\b
\binstagram\.com\b
\binstagram\.com
facebook\.com\/(groups|group\.php|\d+($|\?|\/))
\bfacebook\.com\/(groups|group\.php)\b
\.shop\b
\bblogspot\.com\b
blogspot.com
blogspot\.com
\bissuu\.com\b
\.online\b
\bslideshare\.net
55\.ht
\.club
amazon\.co\.jp
ameblo\.jp
\bgeni\.com
www\.geni\.com
\bcbc\.ca\b
\bads?\b
goodreads\.com
\bfandom\.com
fandom\.com
\bfandom\.com\b
\bthemoviedb\.org\b
\blinkedin\.com\b
open\.spotify\.com
\bprabook\.com
\bamazon\.co\.uk\b
\bsina\.com\b
\bgeneanet\.org
geneanet\.org
\bmetal-archives\.com
\bprotectedplanet\.net\b
casino.*
\bvk\.com\b
\.club\b
(amazon|amzn)\.(com|es|fr|uk)(?!/dp/[0-9]+)
\bamazon\.com\b
archive\.md
myspace\.com
\breddit\.com\b
reddit\.com
\breddit\.com
wikiwix\.com
\bbbfc\.co\.uk
chatgpt
\bmapy\.cz\b
\byandex\.ru\b
\bwordpress\.com\b
cialis
bookmark.*
xxx
\bfuck\b
\bcensus2011\.co\.in\b
\bft\.com
\bvk\.com\.*/
\bvk\.c(c|om)\b
evene\.lefigaro\.fr
\.space\b
\.group\b
\.site\b
\bimgur\.com
lesbian
wellness.*
\byoutu\.be\b
\.fun\b
\.top\b
\.xn--
p[o0]rn
europeana\.eu";

const DOMAIN_ALLOWLIST: &str = r"www.youtube.com
t.me
github.com
medium.com
www.facebook.com
blogspot.com
wordpress.com";

/*
often listed:
- \bchinahighlights\.com\b
- \bthemoviedb\.org\b
- \bnerdwallet\.com\b
- \bfamousbirthdays\.com\b
- \barmy-guide\.com\b
- \bwikiwand\.com\b
- \bfilmreference\.com\b
- \bmydramalist\.com\b
- \bdiigo\.com\b
*/

fn init_logging() {
    let filter = filter::Targets::new()
        .with_default(Level::INFO)
        .with_target("eventsource_client::client", Level::WARN);

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();
}

fn process_ev(sbl_check: &SblCheck, ev: PageLinksChange) {
    let (Some(domain), Some(added_links), Some(performer)) = (
        ev.meta.domain.as_ref(),
        ev.added_links,
        ev.performer.as_ref(),
    ) else {
        return;
    };
    let mut found = false;
    for added_el in added_links
        .into_iter()
        .filter(|link| link.external == Some(true))
    {
        let Some(url) = added_el.link else {
            continue;
        };

        let Ok(url) = Url::parse(&url) else {
            continue;
        };
        let Some(host) = url.host_str() else {
            continue;
        };

        if ev
            .performer
            .as_ref()
            .is_some_and(|p| p.user_is_bot || p.user_edit_count.is_some_and(|ec| ec > 1000))
        {
            continue;
        }

        let mut hits = sbl_check
            .get_hits(url.as_str())
            .expect("regex should not fail");
        hits.retain(|m| match m {
            Hit::GlobalSpamBlacklist { pattern, .. } => {
                !PATTERN_ALLOWLIST.split('\n').any(|p| pattern == p)
            }
            Hit::SpamBlacklist { pattern, .. } => {
                !PATTERN_ALLOWLIST.split('\n').any(|p| pattern == p)
            }
            Hit::BlockedExternalDomain {
                blocked_domain: domain,
                ..
            } => !DOMAIN_ALLOWLIST.split('\n').any(|p| domain == p),
            Hit::SpamWhitelist { .. } => true,
        });
        if hits.iter().all(|m| matches!(m, Hit::SpamWhitelist { .. })) {
            continue;
        }

        if !found {
            let ns = if ev.page_namespace == 0 {
                String::new()
            } else {
                format!("{}:", ev.page_namespace)
            };
            println!(
                "{}{} - https://{domain}/w/index.php?diff={} - {} #edits: {}",
                ns,
                ev.page_title.replace('_', " "),
                ev.rev_id,
                performer.user_text,
                performer
                    .user_edit_count
                    .map_or("?".to_string(), |x| x.to_string()),
            );
            found = true;
        }
        println!("* NEW {url} - {}", ev.database);
        for m in hits {
            println!("  - {m:?}");
        }
        let domain = host.strip_prefix("www.").unwrap_or(host);
        println!("  x - https://spamcheck.toolforge.org/by-domain?q={domain}",);
    }
    if found {
        println!();
    }
}

struct Cache<K: Copy, V> {
    map: HashMap<K, V>,
    deque: VecDeque<K>,
    max_size: usize,
}

impl<K: Copy + Eq + PartialEq + Hash, V> Cache<K, V> {
    fn new(max_size: usize) -> Self {
        Self {
            map: HashMap::new(),
            deque: VecDeque::new(),
            max_size,
        }
    }

    fn get(&self, key: K) -> Option<&V> {
        self.map.get(&key)
    }

    fn insert(&mut self, key: K, value: V) -> Option<V> {
        let mut old = None;
        if self.map.len() >= self.max_size {
            let k = self.deque.pop_front().expect("should exist");
            old = self.map.remove(&k);
        }
        self.deque.push_back(key);
        self.map.insert(key, value);
        old
    }
}

#[allow(clippy::struct_field_names)]
struct SharedData {
    rc_events: Cache<i64, RevisionCreate>,
    change_link_events: Cache<i64, PageLinksChange>,
    processed_link_events: HashSet<i64>,
}

#[must_use]
pub const fn user_agent() -> &'static str {
    "sblwatch/0.0.1 (User:Count Count)"
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    init_logging();
    info!("Reading spam blacklists...");
    let start = std::time::Instant::now();
    let api_client_service = api_client::new_sbl_api_client_service(user_agent())?;
    let sbl_check = SblCheck::new(&api_client_service).await?;
    info!(
        "Read spam blacklists in {:.2}s",
        start.elapsed().as_secs_f32()
    );

    let shared_data = RefCell::new(SharedData {
        rc_events: Cache::new(10000),
        change_link_events: Cache::new(10000),
        processed_link_events: HashSet::new(),
    });

    info!("Listening for events...");

    wikimedia_eventstreams::Builder::new()
        .user_agent(user_agent())
        .build_for_multiple(
            MultipleStreams::none()
                .add::<PageLinksChange>()
                .add::<RevisionCreate>()
                .add::<PageChange>(),
        )
        .expect("should work")
        .for_each(|ev| async {
            match ev {
                Ok(ev) => match ev.data {
                    Any::PageLinksChange(ev) => {
                        {
                            let mut shared_data = shared_data.borrow_mut();
                            if !shared_data.processed_link_events.contains(&ev.rev_id) {
                                shared_data.change_link_events.insert(ev.rev_id, ev);
                                return;
                            }
                        }
                        process_ev(&sbl_check, ev);
                    }
                    Any::RevisionCreate(ev) => {
                        let add_ev = {
                            let mut shared_data = shared_data.borrow_mut();
                            let Some(add_ev) = shared_data.change_link_events.get(ev.rev_id) else {
                                shared_data.rc_events.insert(ev.rev_id, ev);
                                return;
                            };
                            add_ev.clone()
                        };
                        process_ev(&sbl_check, add_ev);
                        shared_data
                            .borrow_mut()
                            .processed_link_events
                            .insert(ev.rev_id);
                    }
                    Any::PageChange(ev) => {
                        sbl_check
                            .page_changed(&ev, &api_client_service)
                            .await
                            .expect("Failed to update sbl");
                    }
                    _ => {
                        unreachable!("unexpected event");
                    }
                },
                Err(e) => println!("Error: {e:?}"),
            }
        })
        .await;

    Ok(())
}
