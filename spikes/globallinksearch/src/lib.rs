use std::{
    net::{Ipv4Addr, Ipv6Addr},
    str::FromStr,
    time::Duration,
};

use futures::future::try_join_all;
use http::uri::Uri;
use sqlx::{
    MySql, Pool,
    mysql::{MySqlConnectOptions, MySqlPoolOptions},
};

/*
TODO:
- handle invalid urls in domain part, stored in db urlencoded
*/

pub struct LinkSearcher {
    pools: Vec<Pool<MySql>>,
    wikis: Vec<Wiki>,
}

#[allow(dead_code)]
#[derive(Debug, sqlx::FromRow)]
struct Wiki {
    dbname: String,
    slice: String,
    lang: String,
    family: String,
    url: String,
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("database error")]
    Database(#[from] sqlx::Error),
}

const WEB_SLICE_URLS: [&str; 8] = [
    "mysql://s1.web.db.svc.wikimedia.cloud",
    "mysql://s2.web.db.svc.wikimedia.cloud",
    "mysql://s3.web.db.svc.wikimedia.cloud",
    "mysql://s4.web.db.svc.wikimedia.cloud",
    "mysql://s5.web.db.svc.wikimedia.cloud",
    "mysql://s6.web.db.svc.wikimedia.cloud",
    "mysql://s7.web.db.svc.wikimedia.cloud",
    "mysql://s8.web.db.svc.wikimedia.cloud",
];

const ANALYTICS_SLICE_URLS: [&str; 8] = [
    "mysql://s1.analytics.db.svc.wikimedia.cloud",
    "mysql://s2.analytics.db.svc.wikimedia.cloud",
    "mysql://s3.analytics.db.svc.wikimedia.cloud",
    "mysql://s4.analytics.db.svc.wikimedia.cloud",
    "mysql://s5.analytics.db.svc.wikimedia.cloud",
    "mysql://s6.analytics.db.svc.wikimedia.cloud",
    "mysql://s7.analytics.db.svc.wikimedia.cloud",
    "mysql://s8.analytics.db.svc.wikimedia.cloud",
];

const META_POOL: usize = 6;

#[derive(Debug, Default)]
pub struct Builder {
    slice_urls: Vec<String>,
    max_connections: u32,
    idle_timeout: Duration,
}

impl Builder {
    pub fn new() -> Self {
        Self {
            slice_urls: ANALYTICS_SLICE_URLS.iter().map(|s| s.to_string()).collect(),
            max_connections: 3,
            idle_timeout: Duration::from_secs(1),
        }
    }

    pub fn with_custom_connection(mut self, slice_urls: [String; 8]) -> Self {
        self.slice_urls = slice_urls.into_iter().collect();
        self
    }

    pub fn with_analytics_db(mut self) -> Self {
        self.slice_urls = ANALYTICS_SLICE_URLS.iter().map(|s| s.to_string()).collect();
        self
    }

    pub fn with_web_db(mut self) -> Self {
        self.slice_urls = WEB_SLICE_URLS.iter().map(|s| s.to_string()).collect();
        self
    }

    pub fn with_max_connections(mut self, max: u32) -> Self {
        assert!(max > 0, "max connections must be greater than 0");
        self.max_connections = max;
        self
    }

    pub async fn build(self, username: &str, password: &str) -> Result<LinkSearcher, Error> {
        LinkSearcher::new(
            self.slice_urls,
            self.max_connections,
            self.idle_timeout,
            username,
            password,
        )
        .await
    }
}

impl LinkSearcher {
    async fn new(
        slice_urls: Vec<String>,
        max_connections: u32,
        idle_timeout: Duration,
        username: &str,
        password: &str,
    ) -> Result<Self, Error> {
        let mut pool_futures = Vec::new();
        for url in slice_urls {
            let conn = url
                .parse::<MySqlConnectOptions>()?
                .username(username)
                .password(password);
            let pool_future = MySqlPoolOptions::new()
                .max_connections(max_connections)
                .min_connections(0)
                .idle_timeout(idle_timeout)
                .connect_with(conn);
            pool_futures.push(pool_future);
        }
        let pools = try_join_all(pool_futures).await?;

        let wikis = sqlx::query_as::<_, Wiki>(
            "SELECT dbname, slice, name, lang, family, url FROM meta_p.wiki WHERE is_closed<>1 ORDER BY slice ASC",
        )
        .fetch_all(&pools[META_POOL])
        .await?;

        Ok(Self { pools, wikis })
    }

    pub async fn search(&self, host: &str) -> Result<Vec<ExternalLink>, Error> {
        let host = indexify(host)
            .replace('!', "!!")
            .replace('%', "!%")
            .replace('_', "!_")
            .replace('[', "![");
        // host.push('%');
        let mut query_strings = vec![Vec::new(); self.pools.len()];
        for wiki in &self.wikis {
            if matches!(wiki.dbname.as_str(), "testwiki") {
                continue;
            }
            let slice = get_slice(&wiki.slice);
            query_strings[slice].push(format!(
                    "(SELECT '{dbname}' as dbname, page_namespace, el_from as page_id, CONVERT(page_title USING utf8) AS page_title, CONVERT(el_to_domain_index USING utf8) AS domain_index, CONVERT(el_to_path USING utf8) AS path FROM {dbname}_p.externallinks, {dbname}_p.page  WHERE (el_to_domain_index LIKE CONCAT('https://', ?) ESCAPE '!' OR el_to_domain_index LIKE CONCAT('http://', ?) ESCAPE '!') AND el_from = page_id LIMIT 10)\n" ,
                    dbname = wiki.dbname,
                ));
        }

        let mut combined_query_strings = Vec::new();
        let mut futures = Vec::new();
        for query_strings in query_strings.iter() {
            let query_string = query_strings.join("    UNION\n");
            combined_query_strings.push((query_string, query_strings.len()));
        }

        #[derive(Debug, sqlx::FromRow)]
        struct ExternalLinkRow {
            dbname: String,
            page_namespace: i32,
            page_title: String,
            page_id: u32,
            domain_index: String,
            path: String,
        }

        for (i, query_string) in combined_query_strings.iter().enumerate() {
            if query_string.1 == 0 {
                continue;
            }
            let pool = &self.pools[i];
            let mut query = sqlx::query_as::<_, ExternalLinkRow>(&query_string.0);
            for _ in 0..query_string.1 {
                query = query.bind(&host).bind(&host);
            }
            let future = query.fetch_all(pool);
            futures.push(future);
        }
        let vecs = try_join_all(futures).await?;
        let mut res = Vec::new();
        for vec in vecs {
            for el in vec {
                res.push(ExternalLink {
                    dbname: el.dbname,
                    page_namespace: el.page_namespace,
                    page_title: el.page_title,
                    page_id: el.page_id,
                    url: unindexify_uri(&el.domain_index, &el.path),
                });
            }
        }
        Ok(res)
    }

    #[cfg(test)]
    fn num_idle(&self) -> usize {
        let mut res = 0;
        for pool in &self.pools {
            res += pool.num_idle()
        }
        res
    }
}

#[derive(Debug, sqlx::FromRow)]
pub struct ExternalLink {
    pub dbname: String,
    pub page_namespace: i32,
    pub page_title: String,
    pub page_id: u32,
    pub url: String,
}

fn indexify(host: &str) -> String {
    if Ipv4Addr::from_str(host).is_ok() {
        return format!("V4.{}", host) + ".";
    } else if host.starts_with('[') && host.ends_with(']') {
        if let Ok(ip) = Ipv6Addr::from_str(&host[1..host.len() - 1]) {
            return format!("V6.{}", ipv6_in_full_dotted(ip)) + ".";
        }
    }
    host.split('.').rev().collect::<Vec<&str>>().join(".") + "."
}

fn ipv6_in_full_dotted(ip: Ipv6Addr) -> String {
    ip.segments()
        .iter()
        .map(|o| format!("{:X}", o))
        .collect::<Vec<String>>()
        .join(".")
}

fn unindexify(host: &str) -> String {
    if host.starts_with("V4.") {
        if let Ok(ip_addr) = Ipv4Addr::from_str(&host[3..host.len() - 1]) {
            return ip_addr.to_string();
        }
    }
    if host.starts_with("V6.") {
        if let Ok(ip_addr) = Ipv6Addr::from_str(
            host[3..host.len() - 1]
                .to_string()
                .replace('.', ":")
                .as_str(),
        ) {
            return "[".to_string() + &ip_addr.to_string() + "]";
        }
    }
    let mut res = host.split('.').rev().collect::<Vec<&str>>().join(".");
    if res.get(0..1) == Some(".") {
        res.remove(0);
    }
    res
}

fn unindexify_uri(prot_host: &str, path: &str) -> String {
    let uri = prot_host
        .parse::<Uri>()
        .unwrap_or_else(|_| panic!("invalid URI: {}", prot_host));

    let host = uri.host().expect("URL without host");
    let mut authority = unindexify(host);
    if let Some(port) = uri.port() {
        authority.push(':');
        authority.push_str(port.as_str());
    }
    let scheme = uri.scheme_str().expect("URL without scheme");
    let builder = Uri::builder()
        .scheme(scheme)
        .authority(authority.as_str())
        .path_and_query("");
    // path can contain invalid characters, so we need to append it manually
    let mut res = builder.build().expect("failed to build URI").to_string();
    res.push_str(path);
    res
}

fn get_slice(slice: &str) -> usize {
    if slice.starts_with('s') && slice.ends_with(".labsdb") && slice.len() == 9 {
        let slice = slice[1..2].parse::<usize>().expect("invalid slice");
        if (1..=8).contains(&slice) {
            return slice - 1;
        }
    }
    panic!("invalid slice");
}

#[cfg(test)]
mod test {
    use std::{fs::read_to_string, time::Duration};

    use tokio::time::sleep;

    use super::*;

    #[derive(Debug, serde::Deserialize)]
    struct Config {
        urls: [String; 8],
        username: String,
        password: String,
    }

    async fn create() -> LinkSearcher {
        let config = toml::from_str::<Config>(
            &read_to_string("globallinksearch.toml").expect("globallinksearch.toml should exist"),
        )
        .expect("config.toml should be valid");
        Builder::new()
            .with_custom_connection(config.urls)
            .build(&config.username, &config.password)
            .await
            .expect("should connect to db")
    }

    async fn test_search(domain: &str) -> Vec<ExternalLink> {
        create()
            .await
            .search(domain)
            .await
            .expect("search should work")
    }

    async fn test_search_count(domain: &str, min: usize) {
        let els = test_search(domain).await;
        assert!(
            els.len() >= min,
            "should find at least {} links, found {}",
            min,
            els.len()
        );
        els.first().unwrap().url.parse::<Uri>().unwrap();
    }

    #[ignore]
    #[tokio::test]
    async fn test_domain() {
        test_search_count("www.bbc.co.uk", 100).await;
    }

    #[ignore]
    #[tokio::test]
    async fn test_domain_2() {
        test_search_count("www.meteofrance.com", 100).await;
    }

    #[ignore]
    #[tokio::test]
    async fn test_domain_3() {
        test_search_count("nepaldesk.com", 2).await;
    }

    #[ignore]
    #[tokio::test]
    async fn test_ipv4() {
        test_search_count("192.0.2.0", 1).await;
    }

    #[ignore]
    #[tokio::test]
    async fn test_ipv6() {
        test_search_count("[2001:db8::]", 1).await;
    }

    #[ignore]
    #[tokio::test]
    async fn test_not_exist() {
        let count = test_search("does.not.exist").await.len();
        assert_eq!(count, 0);
    }

    #[ignore]
    #[tokio::test]
    async fn test_idle() {
        let ls = create().await;
        ls.search("does.not.exist")
            .await
            .expect("search should work");
        sleep(Duration::from_secs(3)).await;
        assert_eq!(ls.num_idle(), 0)
    }
}
