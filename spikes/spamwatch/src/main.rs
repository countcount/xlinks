use std::{
    cell::RefCell,
    collections::{HashMap, HashSet, VecDeque},
    fs::read_to_string,
    hash::Hash,
};

use futures::StreamExt;
use http::Uri;
use tracing::Level;
use tracing_subscriber::{filter, prelude::*};
use wikimedia_events::{
    Any,
    page_links_change::{PageLinksChange, PageLinksChangePerformer},
    revision_create::RevisionCreate,
    revision_tags_change::RevisionTagsChange,
};
use wikimedia_eventstreams::MultipleStreams;

/*
 * TODO:
 * - check last revision timestamp
 * - rev id -> (wiki, rev_id) in cache
 */

fn init_logging() {
    let filter = filter::Targets::new()
        .with_default(Level::ERROR)
        .with_target("eventsource_client::client", Level::WARN);

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();
}

fn is_in_trusted_group(performer: &PageLinksChangePerformer) -> bool {
    performer
        .user_groups
        .iter()
        .any(|group| matches!(group.as_str(), "sysop" | "rollbacker" | "patroller"))
}

fn is_trusted_user(performer: &PageLinksChangePerformer) -> bool {
    performer.user_is_bot
        || (performer.user_id.is_some() && performer.user_edit_count.is_some_and(|ec| ec > 200))
        || is_in_trusted_group(performer)
}

async fn process_ev(
    link_to_wiki: &RefCell<HashMap<String, u32>>,
    gls: &globallinksearch::LinkSearcher,
    ev: PageLinksChange,
) {
    // if ev.page_id == 60585166 {
    //     for link in ev.added_links.as_ref().unwrap() {
    //         if link.external == Some(true) {
    //             println!("{:?}", link);
    //         }
    //     }
    // }

    // if true {
    //     return;
    // }

    let (Some(domain), Some(added_links), Some(performer)) = (
        ev.meta.domain.as_ref(),
        ev.added_links,
        ev.performer.as_ref(),
    ) else {
        return;
    };
    if is_trusted_user(performer) {
        return;
    }

    if !matches!(
        domain.as_str(),
        "en.wikipedia.org"
            | "simple.wikipedia.org"
            | "de.wikipedia.org"
            | "es.wikipedia.org"
            | "meta.wikimedia.org"
            | "wikidata.org"
    ) {
        return;
    }

    if ev.page_title == "Wikipedia:Administrator intervention against vandalism" {
        return;
    }
    let mut found = false;
    for added_el in added_links
        .into_iter()
        .filter(|link| link.external == Some(true))
    {
        let Some(url) = added_el.link else {
            continue;
        };

        let Ok(uri) = Uri::try_from(&url) else {
            continue;
        };
        let Some(host) = uri.host() else {
            continue;
        };

        let Some(tld) = host.split('.').next_back() else {
            continue;
        };

        let Some(dom) = host.split('.').nth_back(1) else {
            continue;
        };

        if matches!(tld, "edu" | "gov" | "mil") {
            continue;
        }

        if matches!(dom, "edu" | "gov" | "mil" | "ac") {
            continue;
        }

        let mut els = match gls.search(host).await {
            Ok(els) => els,
            Err(e) => {
                eprintln!("Could not get links for {}: {e:?}", host);
                continue;
            }
        };

        let mut own_count = 0;
        let mut other_count = 0;
        if !els.is_empty() {
            for i in (0..els.len()).rev() {
                if els[i].dbname != ev.database {
                    other_count += 1;
                } else if els[i].page_id as i64 != ev.page_id {
                    own_count += 1;
                } else {
                    els.remove(i);
                }
            }
        }

        if els.len() >= 10 {
            continue;
        }

        if !found {
            let ns = if ev.page_namespace == 0 {
                String::new()
            } else {
                format!("{}:", ev.page_namespace)
            };
            println!(
                "{}{} - https://{domain}/w/index.php?diff={} - {} #edits: {}",
                ns,
                ev.page_title.replace('_', " "),
                ev.rev_id,
                performer.user_text,
                performer
                    .user_edit_count
                    .map(|x| x.to_string())
                    .unwrap_or("?".to_string()),
            );
            found = true;
        }
        let mut link_to_wiki = link_to_wiki.borrow_mut();
        let e = link_to_wiki.entry(url);
        let domain = host.strip_prefix("www.").unwrap_or(host);
        match e {
            std::collections::hash_map::Entry::Occupied(mut o) => {
                *o.get_mut() += 1;
                println!(
                    "* MULTI {} ({}) - {}: {own_count} - other: {other_count}",
                    o.key(),
                    o.get(),
                    ev.database,
                );
                println!("  https://spamcheck.toolforge.org/by-domain?q={domain}",);
            }
            std::collections::hash_map::Entry::Vacant(v) => {
                println!(
                    "* NEW {} - {}: {own_count} - other: {other_count}",
                    v.key(),
                    ev.database
                );
                v.insert(1);
                println!("  https://spamcheck.toolforge.org/by-domain?q={domain}",);
            }
        }
    }
    if found {
        println!();
    }
}

struct Cache<K: Copy, V> {
    map: HashMap<K, V>,
    deque: VecDeque<K>,
    max_size: usize,
}

impl<K: Copy + Eq + PartialEq + Hash, V> Cache<K, V> {
    fn new(max_size: usize) -> Self {
        Self {
            map: HashMap::new(),
            deque: VecDeque::new(),
            max_size,
        }
    }

    fn get(&self, key: K) -> Option<&V> {
        self.map.get(&key)
    }

    fn insert(&mut self, key: K, value: V) -> Option<V> {
        let mut old = None;
        if self.map.len() >= self.max_size {
            let k = self.deque.pop_front().unwrap();
            old = self.map.remove(&k);
        }
        self.deque.push_back(key);
        self.map.insert(key, value);
        old
    }
}

struct SharedData {
    rc_events: Cache<i64, RevisionCreate>,
    change_link_events: Cache<i64, PageLinksChange>,
    processed_link_events: HashSet<i64>,
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    init_logging();

    #[derive(Debug, serde::Deserialize)]
    struct Config {
        urls: [String; 8],
        username: String,
        password: String,
    }

    let config = toml::from_str::<Config>(
        &read_to_string("spamwatch.toml").expect("spamwatch.toml should exist"),
    )
    .expect("spamwatch.toml should be valid");
    let gls = globallinksearch::Builder::new()
        .with_custom_connection(config.urls)
        .build(&config.username, &config.password)
        .await?;
    let link_to_wiki: RefCell<HashMap<String, u32>> = RefCell::new(HashMap::new());

    let shared_data = RefCell::new(SharedData {
        rc_events: Cache::new(10000),
        change_link_events: Cache::new(10000),
        processed_link_events: HashSet::new(),
    });

    wikimedia_eventstreams::Builder::new()
        .user_agent("el/0.0.1")
        .build_for_multiple(
            MultipleStreams::none()
                .add::<PageLinksChange>()
                .add::<RevisionCreate>()
                .add::<RevisionTagsChange>(),
        )
        .unwrap()
        .for_each(|ev| async {
            match ev {
                Ok(ev) => match ev.data {
                    Any::PageLinksChange(ev) => {
                        {
                            let mut shared_data = shared_data.borrow_mut();
                            if !shared_data.processed_link_events.contains(&ev.rev_id) {
                                shared_data.change_link_events.insert(ev.rev_id, ev);
                                return;
                            }
                        }
                        process_ev(&link_to_wiki, &gls, ev).await;
                    }
                    Any::RevisionCreate(ev) => {
                        let add_ev = {
                            let mut shared_data = shared_data.borrow_mut();
                            let Some(add_ev) = shared_data.change_link_events.get(ev.rev_id) else {
                                shared_data.rc_events.insert(ev.rev_id, ev);
                                return;
                            };
                            add_ev.clone()
                        };
                        process_ev(&link_to_wiki, &gls, add_ev).await;
                        shared_data
                            .borrow_mut()
                            .processed_link_events
                            .insert(ev.rev_id);
                    }
                    Any::RevisionTagsChange(_ev) => {
                        // let (Some(tags), Some(domain)) = (ev.tags, ev.meta.domain) else {
                        //     return;
                        // };
                        // if !tags.iter().any(|tag| tag == "mw-reverted") {
                        //     return;
                        // }
                        // println!(
                        //     "REVERT of https://{domain}/w/index.php?diff={rev_id}",
                        //     rev_id = ev.rev_id,
                        // );
                    }
                    _ => {
                        unreachable!("unexpected event");
                    }
                },
                Err(e) => println!("Error: {:?}", e),
            }
        })
        .await;

    Ok(())
}
