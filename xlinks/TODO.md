### TODO
- blacklist common domains but used for spamming such as
  - sites.google.com
  - Same for azure?
- script to find discrepancies with externallinks table
- fix one-of-usertext-localuserid-ip invariant
- publish stream
  - get global user trusted, xwiki/samewiki link count, sbl'ed in stream
- test with extra table containing all subdomains per host for faster queries
- add autoincremented primary key for revision table
- partitioned tables (or manually partitioned) containing *all* addition/removals even allowlisted ones
- treat http/https conversions differently, maybe
- update allowlisted hosts from externallinks table with new column hst_page_count and hst_page_count_updated and blocklist if necessary
- allowlisted_changed field
- add hst_removed_as_spam field

- idea: scan added text for urls outside of <a>s and check for SBL
- test spamwatch stream additions for enwp-sbled urls

- record links in edit summaries



DB update:
- rev_user_edits
- rev_user_trusted (local groups, global groups if available)

- hst_removed_all if all links for this host have been removed
- hst_added_new if links for this hosts have been added for the first time
- hst_removed_as_spam
- hst_removed_all_as_spam

- hst_reverted, hst_reverted_all: on revert, record reverted count

- hst_added_count -> hst_added
- hst_removed_count -> hst_removed

## BUGS:

## MAYBE NEVER:
- fix one-of-usertext-localuserid-ip invariant
- check if email is correctly stored
- rename fk constraints
- fix high counters in usertext tables
- performance stats ( time spent)
- add tracing to functions for debugging
- refactor: extract in-flight data structure
- graceful shutdown with drain via redis/webinterface
- delete/restore page timestamp in db (events are not necessarily in order)
- split streams so backlog can be processed in order
- refactor: use newtypes for user ids
- refactor: break up shared_data for better encapsulation/testing
- use jemalloc/mimalloc?
