use anyhow::{Result, anyhow, bail};
use reqwest::Response;
use shared::api_client::{ApiClientService, Error, get_with_retry, get_with_retry_resp};

pub async fn get_page_html_resp(
    domain: &str,
    _page_title: &str,
    rev_id: i64,
    rcs: ApiClientService,
) -> Result<Response, Error> {
    //https://en.wikipedia.org/w/rest.php/v1/revision/764138197/html
    let url = format!("https://{domain}/w/rest.php/v1/revision/{rev_id}/html");
    get_with_retry_resp(&url, rcs).await
}

pub async fn get_global_user_id(
    acs: ApiClientService,
    domain: &str,
    local_id: u32,
) -> Result<Option<u32>> {
    #[derive(serde::Deserialize)]
    struct QueryResult {
        query: Query,
    }
    #[derive(serde::Deserialize)]
    struct Query {
        users: Vec<User>,
    }
    #[derive(serde::Deserialize)]
    struct User {
        centralids: Option<CentralIds>,
        missing: Option<String>,
    }
    #[derive(serde::Deserialize)]
    struct CentralIds {
        #[serde(rename = "CentralAuth")]
        central_auth: Option<u32>,
    }

    let url = format!(
        "https://{domain}/w/api.php?action=query&list=users&usprop=centralids&ususerids={local_id}&format=json"
    );
    let text = get_with_retry(&url, acs).await?;
    let res = serde_json::from_str::<QueryResult>(&text)?;
    let user = res
        .query
        .users
        .first()
        .ok_or_else(|| anyhow!("user info not in result"))?;
    if user.missing.is_some() {
        bail!("user not found");
    }
    let Some(centralids) = user.centralids.as_ref() else {
        bail!("user has no centralids");
    };
    Ok(centralids.central_auth)
}

#[cfg(test)]
mod test {
    use shared::api_client::new_action_api_client_service;

    use crate::util::user_agent;

    use super::*;

    #[tokio::test]
    #[allow(clippy::unreadable_literal)]
    async fn test_attached_user() -> Result<()> {
        assert_eq!(
            get_global_user_id(
                new_action_api_client_service(user_agent())?,
                "meta.wikimedia.org",
                17151482
            )
            .await?,
            Some(51590292)
        );
        Ok(())
    }

    #[tokio::test]
    async fn test_local_only_user() -> Result<()> {
        assert_eq!(
            get_global_user_id(
                new_action_api_client_service(user_agent())?,
                "id.wikimedia.org",
                278
            )
            .await?,
            None
        );
        Ok(())
    }
}
