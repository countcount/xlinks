use std::{collections::HashSet, str::from_utf8, sync::Arc};

use quick_xml::{Error, Reader};
use regex_macro::regex;
use shared::normalize_host;
use tokio::io::AsyncBufRead;
use tracing::warn;

use crate::util::replace_in_place;

pub async fn get_links<T>(html: T) -> Result<HashSet<String>, Arc<std::io::Error>>
where
    T: AsyncBufRead + Unpin,
{
    let mut res = HashSet::new();
    let mut reader = Reader::from_reader(html);
    reader.config_mut().trim_text(true);
    reader.config_mut().enable_all_checks(false);
    let mut buf = Vec::with_capacity(256);
    loop {
        use quick_xml::events::Event;
        buf.clear();
        match reader.read_event_into_async(&mut buf).await {
            Ok(Event::Start(s)) if s.local_name().as_ref() == b"a".as_ref() => {
                let mw_ext_link = s
                    .try_get_attribute(b"rel")
                    .expect(
                        "try_get_attribute(b\"rel\") should succeed on well-formed Wikimedia HTML",
                    )
                    .is_some_and(|a| a.value.starts_with(b"mw:ExtLink".as_ref()));
                let external = s.try_get_attribute(b"class").is_ok_and(|attr| {
                    attr.is_some_and(|class| {
                        class.value.split(|&x| x == b' ').any(|c| c == b"external")
                    })
                });
                if !mw_ext_link && !external {
                    continue;
                }
                let Some(href_attr) = s.try_get_attribute("href").expect(
                    "try_get_attribute(b\"href\") should succeed on well-formed Wikimedia HTML",
                ) else {
                    break;
                };
                let href = from_utf8(href_attr.value.as_ref()).expect("href should be valid UTF-8");
                if let Some(url) = normalize_url(href) {
                    res.insert(url);
                }
            }
            Ok(Event::Eof) => break,
            Err(Error::Io(e)) => return Err(e),
            _ => {}
        }
    }
    Ok(res)
}

fn normalize_url(url: &str) -> Option<String> {
    // avoid the churn of str::replace()
    let mut url = url.to_string();
    replace_in_place(&mut url, "&amp;", "&");
    let r = regex!(
        "^(?i)(?<scheme>https?://|ftps?://|ircs?://|gopher://|telnet://|mms://|news://|nntp://|redis://|git://|ssh://|sftp://|svn://|cvs://|//|mailto:|sips?:|xmpp:)/*(?<userinfo>[^/@]+@)?(?<host>[^/:]+)(?<port>:[0-9]+)?(?<rest>.*)"
    );
    let Some(caps) = r.captures(&url) else {
        if regex!("^(?i)(https?:)?///*$|^(https?:)?///*:|^mailto:$").is_match(&url) {
            return None;
        }
        if !regex!("^(?i)(bitcoin:|geo:|magnet:|matrix:|news:|sms:|tel:|urn:|worldwind:)")
            .is_match(&url)
        {
            warn!("Uncaptured URL: {}", url);
        }
        return None;
    };
    let scheme = caps["scheme"].to_ascii_lowercase();
    let scheme_ref = if scheme == "//" { "https://" } else { &scheme };
    let host = &caps["host"];
    let nhost = normalize_host(host);
    let host = match nhost.as_ref() {
        Some(h) => h,
        None => host,
    };
    if matches!(scheme.as_str(), "mailto:" | "sip:" | "sips:" | "xmpp")
        && caps.name("userinfo").is_some()
    {
        let mut res = String::with_capacity(url.len());
        res.push_str(scheme_ref);
        res.push_str(&caps["userinfo"].to_ascii_lowercase());
        res.push_str(host);
        res.push_str(caps.name("rest").map_or("", |m| m.as_str()));
        Some(res)
    } else {
        let mut res = String::with_capacity(url.len() + "https:".len());
        res.push_str(scheme_ref);
        res.push_str(host);
        res.push_str(caps.name("rest").map_or("", |m| m.as_str()));
        Some(res)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_url_normalization() {
        let urls = [
            // TODO: ip addresses
            ("https://www.example.com", "https://www.example.com"),
            ("https://www.example.com", "https://www.example.com"),
            ("https://www.example.%6frg", "https://www.example.org"),
            ("https://a@www.example.com", "https://www.example.com"),
            ("https://a@www.example.com", "https://www.example.com"),
            ("https://a:b@www.example.com", "https://www.example.com"),
            ("https://www.example.com:8080", "https://www.example.com"),
            ("hTTps://www.example.com", "https://www.example.com"),
            ("https:///www.example.com", "https://www.example.com"),
            ("https:////www.example.com", "https://www.example.com"),
            ("//www.example.com", "https://www.example.com"),
            ("https://www.eXaMpLe.com", "https://www.example.com"),
            ("https://bücher.de/", "https://bücher.de/"),
            ("https://bücher.рф/", "https://bücher.рф/"),
            ("https://bÜCher.рф/", "https://bücher.рф/"),
            ("https://xn--bcher-kva.de/", "https://bücher.de/"),
            ("https://www.xn--bcher-kva.de/", "https://www.bücher.de/"),
            ("https://xn--bcher-kva.xn--p1ai/", "https://bücher.рф/"),
            ("mailto:user@example.com", "mailto:user@example.com"),
            ("mailto:user@eXaMpLe.com", "mailto:user@example.com"),
            ("mailto:uSeR@example.com", "mailto:user@example.com"),
            (
                "https://spamcheck.toolforge.org/by-domain?q=spamcheck.toolforge.org&amp;verbose",
                "https://spamcheck.toolforge.org/by-domain?q=spamcheck.toolforge.org&verbose",
            ),
        ];
        for (url, expected) in &urls {
            let nurl = normalize_url(url).unwrap_or_else(|| panic!("Failed to normalize {url}"));
            assert_eq!(nurl, *expected);
        }
    }
}
