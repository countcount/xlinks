use std::{
    cell::RefCell,
    collections::HashSet,
    net::{Ipv4Addr, Ipv6Addr},
    sync::{
        Arc,
        atomic::{AtomicBool, AtomicI64, Ordering},
    },
    time::Duration,
};

use anyhow::Result;
use futures::TryStreamExt;
use regex_macro::regex;
use reqwest::{Response, StatusCode};
use sqlx::{MySql, Pool};
use time::{OffsetDateTime, format_description::well_known::Rfc3339};
use tokio::{select, time::sleep};
use tracing::{error, info, warn};
use wikimedia_events::{
    page_delete::PageDelete, page_undelete::PageUndelete, revision_create::RevisionCreate,
    revision_tags_change::RevisionTagsChange,
};

use crate::{
    db::{
        LinkWorkItem, get_or_insert_db_id, get_or_insert_host, get_or_insert_scheme_id,
        get_or_insert_user_text, insert_http_failure, insert_revision_and_links, set_page_deleted,
        set_reverted, update_allowlist, update_last_received,
    },
    mwapi::{get_global_user_id, get_page_html_resp},
    util::{
        Diff, SharedData, diff, is_supported_content_model, may_shrink_rss, print_mem_stats,
        split_str,
    },
};
use shared::{
    api_client::{self},
    indexify,
};

pub async fn process_revision_create(
    ev: RevisionCreate,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<()> {
    static MAX_LAG: AtomicI64 = AtomicI64::new(0);
    static LAGGED_EVENTS: AtomicI64 = AtomicI64::new(0);
    static FIRST_EVENT: AtomicBool = AtomicBool::new(true);
    static BACKLOGGED: AtomicBool = AtomicBool::new(false);

    if ev.meta.domain.as_deref() == Some("canary") {
        return Ok(());
    }

    let dt = OffsetDateTime::parse(
        ev.meta
            .dt
            .as_deref()
            .expect("event should have dt timestamp set"),
        &Rfc3339,
    )
    .expect("revision timestamp should be RFC3339-compliant");
    let lag = (OffsetDateTime::now_utc() - dt).whole_seconds();
    update_last_received(pool, dt).await?;
    if FIRST_EVENT.load(Ordering::Relaxed) && lag >= 10 {
        info!("Initially backlogged: {lag} seconds");
        BACKLOGGED.store(true, Ordering::Relaxed);
    }
    FIRST_EVENT.store(false, Ordering::Relaxed);
    if lag < 10 && BACKLOGGED.load(Ordering::Relaxed) {
        info!("Caught up.");
        BACKLOGGED.store(false, Ordering::Relaxed);
    }
    if lag > 300 {
        LAGGED_EVENTS.fetch_add(1, Ordering::Relaxed);
    }
    MAX_LAG.fetch_max(lag, Ordering::Relaxed);
    select! {
        () = sleep(Duration::from_secs(1200)) => {
            error!("Processing revision timed out after 20 minutes");
        }
        res = process_revision(&ev, pool, shared_data) => { res?; }
    }
    shared_data.borrow_mut().revs_processed += 1;
    if shared_data.borrow().revs_processed % 5000 == 0 {
        let max = MAX_LAG.swap(0, Ordering::Relaxed);
        let lagged_events = LAGGED_EVENTS.swap(0, Ordering::Relaxed);
        if max > 300 {
            info!("Backlogged: {max} seconds, lagged: {lagged_events} / 5000");
            BACKLOGGED.store(true, Ordering::Relaxed);
        }
    }
    if shared_data.borrow().revs_processed % 50_000 == 0 {
        print_stats(shared_data);
        may_shrink_rss();
        print_mem_stats();
    }
    if shared_data.borrow().revs_processed % 500_000 == 0 {
        update_allowlist(pool).await?;
    }
    Ok(())
}

pub async fn process_tags_change(
    ev: RevisionTagsChange,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<()> {
    if ev.meta.domain.as_deref() == Some("canary") {
        return Ok(());
    }

    let Some(tags) = &ev.tags else {
        return Ok(());
    };
    if !tags.iter().any(|tag| tag == "mw-reverted")
        || !is_supported_content_model(ev.rev_content_model.as_deref())
    {
        return Ok(());
    }
    let domain = ev
        .meta
        .domain
        .as_deref()
        .expect("domain should be set in event");
    let db_id = get_or_insert_db_id(&ev.database, domain, pool, shared_data).await?;
    let rev_id = ev.rev_id;
    set_reverted(db_id, rev_id, pool, shared_data).await?;
    Ok(())
}

pub async fn process_page_delete(
    ev: PageDelete,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<(), sqlx::Error> {
    if ev.meta.domain.as_deref() == Some("canary") {
        return Ok(());
    }

    let domain = ev
        .meta
        .domain
        .as_deref()
        .expect("domain should be set in event");
    let db_id = get_or_insert_db_id(&ev.database, domain, pool, shared_data).await?;
    let page_id = ev.page_id;
    set_page_deleted(db_id, page_id, true, pool, shared_data).await?;
    Ok(())
}

pub async fn process_page_undelete(
    ev: PageUndelete,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<(), sqlx::Error> {
    if ev.meta.domain.as_deref() == Some("canary") {
        return Ok(());
    }

    let domain = ev
        .meta
        .domain
        .as_deref()
        .expect("domain should be set in event");
    let db_id = get_or_insert_db_id(&ev.database, domain, pool, shared_data).await?;
    let page_id = ev.page_id;
    set_page_deleted(db_id, page_id, false, pool, shared_data).await?;
    Ok(())
}

#[allow(clippy::too_many_lines)]
pub async fn process_revision(
    ev: &RevisionCreate,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<()> {
    let domain = ev
        .meta
        .domain
        .as_deref()
        .expect("domain should be set in event");
    if !is_supported_content_model(ev.rev_content_model.as_deref()) {
        return Ok(());
    }

    sleep(Duration::from_secs(10)).await;

    let db_id = get_or_insert_db_id(&ev.database, domain, pool, shared_data).await?;

    let diff = {
        let rcs = shared_data.borrow().rest_service.clone();
        let page_title: &str = &ev.page_title;
        let rev_id = ev.rev_id;
        let rev_parent_id = ev.rev_parent_id;
        let new_links = match get_page_html_resp(domain, page_title, rev_id, rcs.clone()).await {
            Ok(resp) => get_links(resp).await?,
            Err(e) => {
                if is_ignored_error_log(&e) {
                    if !is_expected_error(&e) {
                        log_http_failure(pool, db_id, ev.page_id, page_title, rev_id, &e).await?;
                    }
                    return Ok(());
                }
                return Err(e.into());
            }
        };
        let old_links = if let Some(rev_parent_id) = rev_parent_id {
            match get_page_html_resp(domain, page_title, rev_parent_id, rcs.clone()).await {
                Ok(resp) => get_links(resp).await?,
                Err(e) => {
                    if is_ignored_error_log(&e) {
                        if !is_expected_error(&e) {
                            log_http_failure(
                                pool,
                                db_id,
                                ev.page_id,
                                page_title,
                                rev_parent_id,
                                &e,
                            )
                            .await?;
                        }
                        return Ok(());
                    }
                    return Err(e.into());
                }
            }
        } else {
            HashSet::new()
        };
        get_links_diff(old_links, new_links).await
    };

    let mut work_items = Vec::with_capacity(diff.added.len() + diff.removed.len());
    let all_links = diff
        .added
        .iter()
        .map(|l| (l, true))
        .chain(diff.removed.iter().map(|l| (l, false)));
    for (scheme, host, path, added) in all_links.filter_map(|(url, added)| {
        split_url(url).map(|(scheme, host, path)| (scheme, host, path, added))
    }) {
        let scheme_id = get_or_insert_scheme_id(scheme, pool, shared_data).await?;
        let (host_id, allowlisted) = get_or_insert_host(&host, pool, shared_data).await?;
        if allowlisted && added {
            shared_data.borrow_mut().links_allowlisted += 1;
        }
        work_items.push(LinkWorkItem {
            is_added: added,
            is_allowlisted: allowlisted,
            host_id,
            scheme_id,
            path: path.map(|p| p.to_string()),
        });
    }

    if work_items.is_empty() {
        return Ok(());
    }

    work_items.sort_unstable_by_key(|i| i.host_id);
    let wil = &work_items;

    let user_id = ev.performer.as_ref().and_then(|p| p.user_id);
    let user_text = ev.performer.as_ref().and_then(|p| p.user_text.as_deref());
    let user_ipv6 = if user_id.is_none() {
        user_text
            .and_then(|text| text.parse::<Ipv6Addr>().ok())
            .map(|ip| ip.octets())
    } else {
        None
    };
    let user_ipv4 = if user_id.is_none() {
        user_text
            .and_then(|text| text.parse::<Ipv4Addr>().ok())
            .map(u32::from)
    } else {
        None
    };
    let mut global_user_id = None;
    let mut user_text_id = None;
    if work_items
        .iter()
        .any(|wi| wi.is_added && !wi.is_allowlisted)
    {
        if let Some(user_id) = user_id {
            let gid = {
                let rs = shared_data.borrow().action_service.clone();
                get_global_user_id(
                    rs,
                    domain,
                    u32::try_from(user_id).expect("user_id should fit in u32"),
                )
                .await
            };
            match gid {
                Ok(Some(id)) => global_user_id = Some(id),
                Ok(None) => {
                    // local-only user
                }
                Err(e) => {
                    warn!(
                        "Error resolving global id {}@{}: {:?}",
                        user_text.unwrap_or("(no user text"),
                        domain,
                        e
                    );
                }
            }
        } else if let Some(user_text) = user_text {
            if user_ipv6.is_none() {
                user_text_id = Some(get_or_insert_user_text(user_text, pool, shared_data).await?);
            }
        }
    }

    insert_revision_and_links(
        pool,
        ev,
        db_id,
        user_id,
        global_user_id,
        user_ipv6,
        user_ipv4,
        user_text_id,
        shared_data,
        wil,
    )
    .await?;
    Ok(())
}

// ---

fn print_stats(shared_data: &RefCell<SharedData>) {
    info!(
        "revs_processed: {}, revs_added: {}, links_added: {}, links_allowlisted: {}, in flight: {}, deadlocks: {}, uvios: {}",
        shared_data.borrow().revs_processed,
        shared_data.borrow().revs_added,
        shared_data.borrow().links_added,
        shared_data.borrow().links_allowlisted,
        shared_data.borrow().events_in_flight.len(),
        shared_data.borrow().lock_failures,
        shared_data.borrow().unique_vios,
    );
}

async fn log_http_failure(
    pool: &Pool<MySql>,
    db_id: u16,
    page_id: i64,
    page_title: &str,
    rev_id: i64,
    e: &api_client::Error,
) -> Result<()> {
    match e {
        api_client::Error::Reqwest(e) => {
            let etype;
            if e.is_timeout() {
                etype = "timeout";
            } else if e.is_connect() {
                etype = "connect";
            } else if e.is_decode() {
                etype = "decode";
            } else if e.is_redirect() {
                etype = "redirect";
            } else if e.is_builder() {
                etype = "builder";
            } else if e.is_request() {
                etype = "request";
            } else if e.is_body() {
                etype = "body";
            } else {
                etype = "other";
            }
            insert_http_failure(
                pool,
                db_id,
                page_id,
                page_title,
                rev_id,
                etype,
                None,
                Some(split_str(&e.to_string(), 65536).0),
                None,
            )
            .await?;
        }
        api_client::Error::HttpStatus { status, body } => {
            insert_http_failure(
                pool,
                db_id,
                page_id,
                page_title,
                rev_id,
                "status",
                Some(status.as_u16()),
                None,
                body.as_deref().map(|b| split_str(b, 65536).0),
            )
            .await?;
        }
        _ => {}
    }
    Ok(())
}

const fn is_expected_error(e: &api_client::Error) -> bool {
    let api_client::Error::HttpStatus { status, body: _ } = e else {
        return false;
    };
    matches!(*status, StatusCode::NOT_FOUND | StatusCode::FORBIDDEN)
}

#[allow(clippy::cognitive_complexity)] // clippy, you are drunk, go home
fn is_ignored_error_log(e: &api_client::Error) -> bool {
    use api_client::Error;
    match e {
        Error::HttpStatus { status, body } => {
            if matches!(*status, StatusCode::NOT_FOUND | StatusCode::FORBIDDEN) {
                // page deleted or revision deleted/suppressed
                return true;
            }
            if status.is_server_error() {
                // server error (timeout) is somewhat common for huge pages unfortunately
                return true;
            }
            if status.is_client_error() {
                warn!("HTTP: CLIENT error: {status}: {body:?}");
                return true;
            }
            false
        }
        Error::Reqwest(re) => {
            if re.is_timeout() {
                warn!("HTTP: Timeout: {re}");
            } else {
                warn!("HTTP: Other error: {re}");
            }
            true
        }
        _ => false,
    }
}

fn split_url(url: &str) -> Option<(&str, String, Option<&str>)> {
    let regex = regex!("^(?<scheme>[^:]+:/*)(?<host>[^/]+)(?<path>/.*)?$");
    let Some(caps) = regex.captures(url) else {
        warn!("Could not split URL {url}");
        return None;
    };
    let scheme = caps
        .name("scheme")
        .expect("pattern requires scheme to be present")
        .as_str();
    let host = indexify(&caps["host"]);
    let mut path = caps.name("path").map(|m| m.as_str());
    if Some("/") == path {
        path = None;
    }
    Some((scheme, host, path))
}

async fn get_links(resp: Response) -> Result<HashSet<String>, Arc<std::io::Error>> {
    let stream = resp.bytes_stream().map_err(std::io::Error::other);
    let sr = tokio_util::io::StreamReader::new(stream);
    crate::parse::get_links(sr).await
}

async fn get_links_diff(old_links: HashSet<String>, new_links: HashSet<String>) -> Diff<String> {
    let (send, recv) = tokio::sync::oneshot::channel();
    rayon::spawn(move || {
        let diff = diff(&old_links, &new_links);
        let _ = send.send(diff);
    });
    recv.await.expect("Panic in rayon::spawn")
}

#[cfg(test)]
#[allow(clippy::unreadable_literal)]
mod test {
    use crate::{mwapi::get_page_html_resp, new_rest_api_client_service, util::user_agent};
    use shared::api_client::ApiClientService;

    use super::*;

    async fn get_links_diff_online(
        domain: &str,
        page_title: &str,
        rev_id: i64,
        rev_parent_id: Option<i64>,
        rcs: ApiClientService,
    ) -> Result<Diff<String>, anyhow::Error> {
        let new_links =
            get_links(get_page_html_resp(domain, page_title, rev_id, rcs.clone()).await?).await?;
        let old_links = if let Some(rev_parent_id) = rev_parent_id {
            get_links(get_page_html_resp(domain, page_title, rev_parent_id, rcs.clone()).await?)
                .await?
        } else {
            HashSet::new()
        };
        Ok(get_links_diff(old_links, new_links).await)
    }

    #[tokio::test]
    async fn test_parsing_error_one() -> Result<()> {
        #[allow(clippy::unreadable_literal)]
        let Diff { added, removed } = get_links_diff_online(
            "en.wikipedia.org",
            "2023_ICC_Men's_T20_World_Cup_Asia_Qualifier",
            1182808263,
            Some(1182808006),
            new_rest_api_client_service(user_agent())?,
        )
        .await?;
        for l in added {
            println!("added {l}");
        }
        for l in removed {
            println!("removed {l}");
        }
        Ok(())
    }

    #[tokio::test]
    async fn test_links_diff_new() -> Result<()> {
        let mut diff = get_links_diff_online(
            "en.wikipedia.org",
            "User:Count_Count/linktest",
            1232348864,
            None,
            new_rest_api_client_service(user_agent())?,
        )
        .await?;
        assert!(diff.removed.is_empty());
        let added = vec![
            "https://1.2.3.4",
            "https://192.0.2.0",
            "https://?abc",
            "https://[2001:db8::]",
            "https://baaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab.example.com",
            "https://bücher.de/",
            "https://case.example.com",
            "https://countcount.example.com/some/path",
            "https://example.com",
            // "https://href.example.com", // <a href="...">...</a> is ignored by parsoid
            "https://included.example.com",
            "https://marco.example.cow",
            "https://other.example.com",
            "https://quad.example.com/quad/",
            "https://slashslash.example.com",
            "https://triple.example.com/triple/",
            "https://tripleslashslashslash.example.com",
            "https://www.example.com",
            "https://öäü.com",
            "mailto:test@example.com",
        ];
        diff.added.sort();
        assert_eq!(diff.added, added);
        Ok(())
    }

    #[tokio::test]
    async fn test_links_diff_no_change() -> Result<()> {
        let diff = get_links_diff_online(
            "en.wikipedia.org",
            "User:Count_Count/linktest",
            1181323769,
            Some(1181323694),
            new_rest_api_client_service(user_agent())?,
        )
        .await?;
        assert!(diff.removed.is_empty());
        let added = Vec::<String>::new();
        assert_eq!(diff.added, added);
        Ok(())
    }

    #[tokio::test]
    async fn test_links_diff_change() -> Result<()> {
        let diff = get_links_diff_online(
            "en.wikipedia.org",
            "User:Count_Count/linktest",
            1181461344,
            Some(1181353815),
            new_rest_api_client_service(user_agent())?,
        )
        .await?;
        assert!(diff.removed.is_empty());
        let added = vec!["https://www.example.com"];
        assert_eq!(diff.added, added);
        Ok(())
    }

    #[tokio::test]
    async fn test_wikidata_links_change() -> Result<()> {
        let diff = get_links_diff_online(
            "www.wikidata.org",
            "User:XXBlackburnXx/sandbox",
            2170429102,
            None,
            new_rest_api_client_service(user_agent())?,
        )
        .await?;
        assert!(diff.removed.is_empty());
        let added = vec!["https://1717274498.just.a.test/"];
        assert_eq!(diff.added, added);
        Ok(())
    }

    #[tokio::test]
    async fn test_wikidata_links_change_property() -> Result<()> {
        let diff = get_links_diff_online(
            "www.wikidata.org",
            "Q4115189",
            2178471978,
            Some(2178318106),
            new_rest_api_client_service(user_agent())?,
        )
        .await?;
        assert!(diff.removed.is_empty());
        let added = vec!["https://www.spiegel.de"];
        assert_eq!(diff.added, added);
        Ok(())
    }
}
