#![allow(clippy::future_not_send)] // we currently use a single-threaded rt

mod db;
mod mwapi;
mod parse;
mod process_event;
mod util;

use std::{
    cell::RefCell,
    collections::{HashMap, VecDeque},
    env,
    fs::{OpenOptions, read_to_string},
    sync::atomic::AtomicU64,
    time::Duration,
};

use anyhow::Result;
use anyhow::anyhow;
use db::update_last_event_id;
use futures::{FutureExt, StreamExt, TryStreamExt, future::join};
use process_event::process_revision_create;
use shared::api_client::{new_action_api_client_service, new_rest_api_client_service};
use sqlx::{
    MySql, Pool,
    mysql::{MySqlConnectOptions, MySqlDatabaseError, MySqlPoolOptions},
    query,
};
use tokio::{
    select,
    signal::unix::{SignalKind, signal},
    time::sleep,
};
use tracing::{Level, error, info, warn};
use tracing_subscriber::{
    filter::{self, LevelFilter},
    layer::SubscriberExt,
    util::SubscriberInitExt,
};
use util::{SharedData, print_mem_stats, trim_rss};
use wikimedia_events::{
    Any, page_delete::PageDelete, page_undelete::PageUndelete, revision_create::RevisionCreate,
    revision_tags_change::RevisionTagsChange,
};
use wikimedia_eventstreams::MultipleStreams;

use crate::{
    db::get_last_event_id,
    process_event::{process_page_delete, process_page_undelete},
};
use crate::{process_event::process_tags_change, util::user_agent};

fn event_in_flight(id: String, shared_data: &RefCell<SharedData>) {
    shared_data.borrow_mut().events_in_flight.push_back(id);
}

async fn event_retired(
    id: String,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<()> {
    *shared_data
        .borrow_mut()
        .done_events
        .entry(id.clone())
        .or_insert(0) += 1;

    let mut latest_processed = None;
    loop {
        let Some(top_id) = shared_data.borrow().events_in_flight.front().cloned() else {
            // no more events in flight
            break;
        };
        let mut sd = shared_data.borrow_mut();
        let Some(val) = sd.done_events.get_mut(&top_id) else {
            // top_id not yet retirable
            break;
        };
        *val = (*val)
            .checked_sub(1)
            .expect("done event count must always be > 0 when in map");
        if *val == 0 {
            sd.done_events.remove(&top_id);
        }
        latest_processed = sd.events_in_flight.pop_front();
    }
    let Some(latest_processed) = latest_processed else {
        // no events were retired
        return Ok(());
    };
    update_last_event_id(&latest_processed, pool).await?;
    Ok(())
}

fn init_logging() {
    let filter = filter::Targets::new()
        .with_default(Level::INFO)
        .with_target("eventsource_client::client", LevelFilter::WARN)
        .with_target("wikimedia_eventstreams", LevelFilter::WARN)
        .with_target("sqlx::query", LevelFilter::ERROR);

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(filter)
        .init();
}

#[derive(Debug, serde::Deserialize)]
struct Config {
    local_db_url: String,
}

fn load_config() -> Config {
    toml::from_str::<Config>(&read_to_string("xlinks.toml").expect("xlinks.toml should exist"))
        .expect("xlinks.toml should be valid")
}

async fn create_pool(config: &Config) -> Result<Pool<MySql>, anyhow::Error> {
    let conn_opt = config.local_db_url.parse::<MySqlConnectOptions>()?;
    let pool = MySqlPoolOptions::new()
        .max_connections(10)
        .min_connections(0)
        .idle_timeout(Duration::from_millis(1000))
        .acquire_timeout(Duration::from_secs(300))
        .acquire_slow_threshold(Duration::from_secs(10))
        .after_connect(|conn, _| {
            async {
                let q = "SET SESSION idle_transaction_timeout=2";
                query(q).execute(conn).await?;
                Ok(())
            }
            .boxed()
        })
        .connect_with(conn_opt)
        .await?;
    Ok(pool)
}

async fn create_pool_with_retry(config: &Config) -> Result<Pool<MySql>, anyhow::Error> {
    'l: loop {
        let pool = create_pool(config).await?;

        #[allow(clippy::collection_is_never_read)] // retain connections
        let mut conns = Vec::with_capacity(pool.options().get_max_connections() as usize);

        for _ in 0..pool.options().get_max_connections() {
            let conn = pool.acquire().await;
            match conn {
                Ok(conn) => {
                    conns.push(conn);
                }
                Err(e) => {
                    let Some(e) = e
                        .as_database_error()
                        .map(|e| e.downcast_ref::<MySqlDatabaseError>())
                    else {
                        return Err(e.into());
                    };
                    if e.number() == 1203 {
                        // too many user connections
                        info!("Waiting for connections to become available...");
                        conns.clear();
                        pool.close().await;
                        sleep(Duration::from_secs(50)).await;
                        continue 'l;
                    }
                }
            }
        }
        return Ok(pool);
    }
}

fn new_shared_data() -> Result<SharedData, anyhow::Error> {
    Ok(SharedData {
        db_name_to_id: HashMap::new(),
        scheme_to_id: HashMap::new(),
        rest_service: new_rest_api_client_service(user_agent())?,
        action_service: new_action_api_client_service(user_agent())?,
        events_in_flight: VecDeque::new(),
        done_events: HashMap::new(),
        links_added: 0,
        links_allowlisted: 0,
        revs_processed: 0,
        revs_added: 0,
        lock_failures: 0,
        unique_vios: 0,
        shutdown: false,
    })
}

// returns true if the program should restart
async fn main0(pool: &Pool<MySql>) -> Result<bool> {
    info!("Running migrations...");
    sqlx::migrate!().run(pool).await?;

    info!("Listening for events...");
    let shared_data = RefCell::new(new_shared_data()?);
    let mut b = wikimedia_eventstreams::Builder::new().user_agent(user_agent());
    if let Some(lev) = get_last_event_id(pool).await? {
        b = b.last_event_id(&lev);
    }
    let stream = b
        .build_for_multiple(
            MultipleStreams::none()
                .add::<RevisionCreate>()
                .add::<RevisionTagsChange>()
                .add::<PageDelete>()
                .add::<PageUndelete>(),
        )?
        .map_err(|e| anyhow!(e))
        .inspect_ok(|ev| {
            event_in_flight(ev.id.clone().expect("event must have an id"), &shared_data);
            touch_event_processed();
        })
        .take_while(|_| async { !shared_data.borrow().shutdown })
        .try_for_each_concurrent(1000, |ev| async {
            match ev.data {
                Any::RevisionCreate(ev) => {
                    process_revision_create(ev, pool, &shared_data).await?;
                }
                Any::RevisionTagsChange(ev) => {
                    process_tags_change(ev, pool, &shared_data).await?;
                }
                Any::PageDelete(ev) => {
                    process_page_delete(ev, pool, &shared_data).await?;
                }
                Any::PageUndelete(ev) => {
                    process_page_undelete(ev, pool, &shared_data).await?;
                }
                _ => {
                    unreachable!("unexpected event");
                }
            }
            event_retired(ev.id.expect("event must have an id"), pool, &shared_data).await?;
            Ok(())
        });

    let mut sighup = signal(SignalKind::hangup())?;
    let hup_wait = async {
        sighup.recv().await;
        warn!("Received SIGHUP");
        info!("Finishing up...");
        let mut sd = shared_data.borrow_mut();
        sd.shutdown = true;
    };

    let mut sigint = signal(SignalKind::interrupt())?;
    let mut sigterm = signal(SignalKind::terminate())?;
    let joined = join(stream, hup_wait);
    select! {
        _ = sigint.recv() => { warn!("Received SIGINT"); return Ok(false);},
        _ = sigterm.recv() => { warn!("Received SIGTERM"); return Ok(false) },
        (res, ()) = joined => {
            res?;
        }
    }
    return Ok(shared_data.borrow().shutdown);
}

fn touch_event_processed() {
    static LAST_TOUCH: AtomicU64 = AtomicU64::new(0);
    let now = std::time::SystemTime::now()
        .duration_since(std::time::UNIX_EPOCH)
        .expect("current time before epoch")
        .as_secs();
    if now - LAST_TOUCH.load(std::sync::atomic::Ordering::Relaxed) < 10 {
        return;
    }

    LAST_TOUCH.store(now, std::sync::atomic::Ordering::Relaxed);
    let mut temp_dir = env::temp_dir();
    temp_dir.push("event_processed.timestamp");
    OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(&temp_dir)
        .inspect_err(|e| error!("Error touching timestamp file: {:?}", e))
        .ok();
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    init_logging();
    let version = option_env!("CI_COMMIT_TAG");
    if let Some(version) = version {
        info!("Starting {}...", version);
    } else {
        info!("Starting...");
    }
    loop {
        trim_rss();
        print_mem_stats();
        let config = load_config();
        let pool = match create_pool_with_retry(&config).await {
            Ok(pool) => pool,
            Err(e) => {
                error!("Error creating pool (terminating): {:?}", e);
                return;
            }
        };
        match main0(&pool).await {
            Ok(restart) => {
                if !restart {
                    break;
                }
            }
            Err(e) => {
                error!("Error (terminating): {:?}", e);
                break;
            }
        }
        info!("Closing DB connection pool...");
        pool.close().await;
    }
    info!("Exiting.");
}
