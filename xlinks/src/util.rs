use std::collections::{HashMap, HashSet, VecDeque};
use std::hash::Hash;

use libc::{RUSAGE_SELF, getrusage, rusage};
use shared::api_client::ApiClientService;
use tracing::info;

pub struct Diff<T> {
    pub added: Vec<T>,
    pub removed: Vec<T>,
}

pub fn diff<T: Eq + Hash + Clone>(old: &HashSet<T>, new: &HashSet<T>) -> Diff<T> {
    let removed = old.difference(new).cloned().collect::<Vec<_>>();
    let added = new.difference(old).cloned().collect::<Vec<_>>();
    Diff { added, removed }
}

pub fn split_str(s: &str, max: usize) -> (&str, Option<&str>) {
    if s.len() <= max {
        return (s, None);
    }
    let mut i = max;
    while !s.is_char_boundary(i) {
        i -= 1;
    }
    (&s[..i], Some(&s[i..]))
}

// ---

pub struct SharedData {
    pub db_name_to_id: HashMap<String, u16>,
    pub scheme_to_id: HashMap<String, u8>,
    pub rest_service: ApiClientService,
    pub action_service: ApiClientService,
    pub events_in_flight: VecDeque<String>,
    pub done_events: HashMap<String, u32>,
    pub links_added: u32,
    pub links_allowlisted: u32,
    pub revs_processed: u32,
    pub revs_added: u32,
    pub lock_failures: u32,
    pub unique_vios: u32,
    pub shutdown: bool,
}

pub fn is_supported_content_model(content_model: Option<&str>) -> bool {
    matches!(
        content_model,
        Some("wikitext" | "proofread-page" | "wikibase-item")
    )
}

pub const fn user_agent() -> &'static str {
    "xlinks/0.0.1 (User:Count Count)"
}

#[derive(Debug, Copy, Clone)]
struct UsageStats {
    pub max_rss: usize,
    pub rss: Option<usize>,
}

pub fn may_shrink_rss() {
    if let Some(memory_stats) = memory_stats::memory_stats() {
        if memory_stats.physical_mem > 100 * 1024 * 1024 {
            trim_rss();
        }
    }
}

pub fn trim_rss() {
    #[cfg(target_os = "linux")]
    unsafe {
        libc::malloc_trim(0);
    }
}

#[cfg(target_family = "unix")]
fn mem_usage() -> Result<UsageStats, std::io::Error> {
    unsafe {
        let mut rusage: rusage = std::mem::zeroed();
        if getrusage(RUSAGE_SELF, &mut rusage) == 0 {
            #[cfg(target_os = "linux")]
            let max_rss =
                usize::try_from(rusage.ru_maxrss).expect("ru_maxrss should fit in usize") * 1024;
            #[cfg(not(target_os = "linux"))]
            let max_rss = usize::try_from(rusage.ru_maxrss).expect("ru_maxrss should fit in usize");

            let memory_stats = memory_stats::memory_stats();
            Ok(UsageStats {
                max_rss,
                rss: memory_stats.map(|s| s.physical_mem),
            })
        } else {
            Err(std::io::Error::last_os_error())
        }
    }
}

#[allow(clippy::cast_precision_loss)]
pub fn print_mem_stats() {
    let usage = mem_usage().expect("mem_usage should succeed");
    info!(
        "rss: {:.2} MiB, max_rss: {:.2} MiB",
        usage.rss.expect("rss should be present") as f64 / 1024.0 / 1024.0,
        usage.max_rss as f64 / 1024.0 / 1024.0
    );
}

#[cfg(not(target_family = "unix"))]
pub fn mem_usage() -> Result<UsageStats, std::io::Error> {
    Err(std::io::Error::new(
        std::io::ErrorKind::Other,
        "not implemented",
    ))
}

pub fn replace_in_place(original: &mut String, from: &str, to: &str) {
    let mut start = 0;
    while let Some(pos) = original[start..].find(from) {
        original.replace_range(start + pos..start + pos + from.len(), to);
        start += pos + to.len();
    }
}
