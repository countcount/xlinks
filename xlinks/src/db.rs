use std::{cell::RefCell, time::Duration};

use crate::util::{SharedData, split_str};

use futures::Future;
use shared::unindexify;
use sqlx::{
    Acquire, Error, MySql, Pool, Transaction, pool::PoolConnection, query, query_as, query_scalar,
};
use time::{OffsetDateTime, format_description::well_known::Rfc3339};
use tokio::time::sleep;
use tracing::{info, warn};
use wikimedia_events::revision_create::RevisionCreate;

async fn with_retry<F, Fut, R>(
    mut f: F,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
    tag: &str,
    retry_on_unique_violation: bool,
) -> Result<R, Error>
where
    F: FnMut(PoolConnection<MySql>) -> Fut,
    Fut: Future<Output = Result<R, Error>>,
{
    let mut lock_fails = 0;
    let mut io_fails = 0;
    let mut uvio_occurred = false;
    loop {
        let pool_connection = pool.acquire().await?;
        match f(pool_connection).await {
            Ok(r) => return Ok(r),
            Err(Error::Io(e)) => {
                io_fails += 1;
                if io_fails > 3 {
                    return Err(Error::Io(e));
                }
                warn!(
                    "{tag} IO failure #{io_fails} - {e} - retrying...",
                    tag = tag
                );
                sleep(Duration::from_secs(rand::random::<u64>() % 60)).await;
            }
            Err(e) => {
                let Some(db_err) = e.as_database_error() else {
                    return Err(e);
                };
                if db_err.is_unique_violation() && retry_on_unique_violation {
                    shared_data.borrow_mut().unique_vios += 1;
                    if uvio_occurred {
                        return Err(e);
                    }
                    uvio_occurred = true;
                    continue;
                }
                if let Some(c) = db_err.code().filter(|c| c == "40001" || c == "HY000") {
                    shared_data.borrow_mut().lock_failures += 1;
                    lock_fails += 1;
                    if lock_fails > 1 {
                        let typ = if c == "40001" {
                            "deadlock"
                        } else {
                            "lock wait timeout"
                        };
                        warn!("{tag} Serialization failure ({typ}) #{lock_fails} - retrying...");
                    }
                    sleep(Duration::from_secs(rand::random::<u64>() % 60)).await;
                    continue;
                }
                return Err(e);
            }
        }
    }
}

pub async fn get_or_insert_db_id(
    name: &str,
    domain: &str,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<u16, Error> {
    if let Some(id) = shared_data.borrow().db_name_to_id.get(name) {
        return Ok(*id);
    }
    let id = with_retry(
        |mut conn| async move {
            if let Some(id) = query_scalar::<_, u16>("SELECT db_id from db WHERE db_name = ?")
                .bind(name)
                .fetch_optional(&mut *conn)
                .await?
            {
                return Ok(id);
            }
            let mut tx = conn.begin().await?;
            let q = "INSERT INTO db (db_name, db_domain) values (?, ?)";
            let res = query(q).bind(name).bind(domain).execute(&mut *tx).await?;
            tx.commit().await?;
            Ok(u16::try_from(res.last_insert_id()).expect("db_id should fit in u16"))
        },
        pool,
        shared_data,
        "get_or_insert_db_id",
        true,
    )
    .await?;
    shared_data
        .borrow_mut()
        .db_name_to_id
        .insert(name.to_string(), id);
    Ok(id)
}

pub async fn get_or_insert_scheme_id(
    scheme: &str,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<u8, Error> {
    if let Some(id) = shared_data.borrow().scheme_to_id.get(scheme) {
        return Ok(*id);
    }
    let id = with_retry(
        |mut conn| async move {
            if let Some(id) =
                query_scalar::<_, u8>("SELECT sch_id from scheme WHERE sch_scheme = ?")
                    .bind(scheme)
                    .fetch_optional(&mut *conn)
                    .await?
            {
                return Ok(id);
            }
            let mut tx = pool.begin().await?;
            let q = "INSERT INTO scheme (sch_scheme) values (?)";
            let id = u8::try_from(
                query(q)
                    .bind(scheme)
                    .execute(&mut *tx)
                    .await?
                    .last_insert_id(),
            )
            .expect("sch_id should fit in u8");
            tx.commit().await?;
            Ok(id)
        },
        pool,
        shared_data,
        "get_or_insert_scheme_id",
        true,
    )
    .await?;
    shared_data
        .borrow_mut()
        .scheme_to_id
        .insert(scheme.to_string(), id);
    Ok(id)
}

pub async fn get_or_insert_host(
    host: &str,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<(u32, bool), Error> {
    with_retry(
        |mut conn| async move {
            if let Some(res) = query_as::<_, (u32, bool)>(
                "SELECT hst_id, hst_allowlisted from host WHERE hst_host = ?",
            )
            .bind(split_str(host, 255).0)
            .fetch_optional(&mut *conn)
            .await?
            {
                return Ok(res);
            }
            let mut tx = conn.begin().await?;
            let q = "INSERT INTO host (hst_host) values (?)";
            let res = query(q)
                .bind(split_str(host, 255).0)
                .execute(&mut *tx)
                .await?;
            tx.commit().await?;
            Ok((
                u32::try_from(res.last_insert_id()).expect("hst_id should fit in u32"),
                false,
            ))
        },
        pool,
        shared_data,
        "get_or_insert_host",
        true,
    )
    .await
}

pub async fn get_or_insert_user_text(
    user_text: &str,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<u32, Error> {
    with_retry(
        |mut conn| async move {
            if let Some(id) = query_scalar::<_, u32>("SELECT ut_id FROM usertext WHERE ut_text = ?")
                .bind(user_text)
                .fetch_optional(&mut *conn)
                .await?
            {
                return Ok(id);
            }
            let mut tx = conn.begin().await?;
            let res = query("INSERT INTO usertext (ut_text) values (?)")
                .bind(user_text)
                .execute(&mut *tx)
                .await?;
            tx.commit().await?;
            Ok(u32::try_from(res.last_insert_id()).expect("ut_id should fit in u32"))
        },
        pool,
        shared_data,
        "get_or_insert_user_text",
        true,
    )
    .await
}

pub struct LinkWorkItem {
    pub is_added: bool,
    pub is_allowlisted: bool,
    pub host_id: u32,
    pub scheme_id: u8,
    pub path: Option<String>,
}

#[allow(clippy::too_many_arguments)]
pub async fn insert_revision_and_links(
    pool: &Pool<MySql>,
    rev_ev: &RevisionCreate,
    db_id: u16,
    user_id: Option<i64>,
    global_user_id: Option<u32>,
    user_ipv6: Option<[u8; 16]>,
    user_ipv4: Option<u32>,
    user_text_id: Option<u32>,
    shared_data: &RefCell<SharedData>,
    work_items: &Vec<LinkWorkItem>,
) -> Result<(), Error> {
    with_retry(
        |mut conn: PoolConnection<MySql>| async move {
            let mut tx = conn.begin().await?;
            let rev_id = if work_items
                .iter()
                .any(|wi| wi.is_added && !wi.is_allowlisted)
            {
                match insert_revision(
                    rev_ev,
                    db_id,
                    user_id,
                    global_user_id,
                    user_ipv6.as_ref().map(|ip| ip as &[u8]),
                    user_ipv4,
                    user_text_id,
                    &mut tx,
                )
                .await
                {
                    Ok(rev_id) => {
                        shared_data.borrow_mut().revs_added += 1;
                        Some(rev_id)
                    }
                    Err(e) => {
                        if e.as_database_error()
                            .is_some_and(|e| e.is_unique_violation())
                        {
                            return Ok(());
                        }
                        return Err(e);
                    }
                }
            } else {
                None
            };
            for wi in work_items {
                if wi.is_added {
                    // increase first to acquire an exclusive lock on the host row
                    increase_added_count(wi.host_id, &mut tx).await?;
                    if !wi.is_allowlisted {
                        insert_added_link(
                            rev_id.expect("rev_id should be set"),
                            wi.scheme_id,
                            wi.host_id,
                            wi.path.as_deref(),
                            &mut tx,
                        )
                        .await?;
                        shared_data.borrow_mut().links_added += 1;
                    }
                } else {
                    increase_removed_count(wi.host_id, &mut tx).await?;
                }
            }
            tx.commit().await?;
            let dt = OffsetDateTime::parse(
                rev_ev
                    .meta
                    .dt
                    .as_deref()
                    .expect("event should have dt timestamp set"),
                &Rfc3339,
            )
            .expect("revision timestamp should be RFC3339-compliant");
            update_last_processed(&mut conn, dt).await?;
            Ok(())
        },
        pool,
        shared_data,
        "insert_revision_and_links",
        false,
    )
    .await
}

pub async fn set_page_deleted(
    db_id: u16,
    page_id: i64,
    deleted: bool,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<(), Error> {
    with_retry(
        |mut conn| async move {
            let mut tx = conn.begin().await?;
            let q = "
        UPDATE revision IGNORE INDEX (PRIMARY)
        SET rev_page_deleted = ?
        WHERE rev_db = ? AND rev_page = ? AND rev_page_deleted = ?";

            query(q)
                .bind(deleted)
                .bind(db_id)
                .bind(page_id)
                .bind(!deleted)
                .execute(&mut *tx)
                .await?;
            tx.commit().await?;
            Ok(())
        },
        pool,
        shared_data,
        "set_page_deleted",
        false,
    )
    .await?;
    Ok(())
}

pub async fn set_reverted(
    db_id: u16,
    rev_id: i64,
    pool: &Pool<MySql>,
    shared_data: &RefCell<SharedData>,
) -> Result<(), Error> {
    with_retry(
        |mut conn: PoolConnection<MySql>| async move {
            let mut tx = conn.begin().await?;
            let q = "
            UPDATE revision
            SET rev_reverted = 1
            WHERE rev_db = ? AND rev_revision = ? AND rev_reverted = 0";
            query(q).bind(db_id).bind(rev_id).execute(&mut *tx).await?;
            tx.commit().await?;
            Ok(())
        },
        pool,
        shared_data,
        "set_reverted",
        false,
    )
    .await?;
    Ok(())
}

pub async fn get_last_event_id(pool: &Pool<MySql>) -> Result<Option<String>, Error> {
    let mut conn = pool.acquire().await?;
    let res = sqlx::query_scalar::<_, String>(
        "SELECT CONVERT(le_ev USING utf8mb4) FROM lastevent WHERE le_ev IS NOT NULL",
    )
    .fetch_optional(&mut *conn)
    .await?;
    Ok(res)
}

pub async fn update_last_event_id(latest_processed: &str, pool: &Pool<MySql>) -> Result<(), Error> {
    let mut tx = pool.begin().await?;
    let query = "UPDATE lastevent SET le_ev = ?";
    sqlx::query(query)
        .bind(latest_processed)
        .execute(&mut *tx)
        .await?;
    tx.commit().await?;
    Ok(())
}

pub async fn update_last_received(pool: &Pool<MySql>, evt: OffsetDateTime) -> Result<(), Error> {
    let mut tx = pool.begin().await?;
    let query = "UPDATE lastprocessed SET lp_time = ? where lp_type = 'received'";
    sqlx::query(query).bind(evt).execute(&mut *tx).await?;
    tx.commit().await?;
    Ok(())
}

pub async fn update_last_processed(
    conn: &mut PoolConnection<MySql>,
    evt: OffsetDateTime,
) -> Result<(), Error> {
    let mut tx = (&mut *conn).begin().await?;
    let query = "UPDATE lastprocessed SET lp_time = ? where lp_type = 'processed'";
    sqlx::query(query).bind(evt).execute(&mut *tx).await?;
    tx.commit().await?;
    Ok(())
}

pub async fn update_allowlist(pool: &Pool<MySql>) -> Result<(), Error> {
    #[derive(Debug, sqlx::FromRow)]
    #[allow(clippy::struct_field_names)]
    struct Host {
        hst_id: u32,
        hst_host: String,
        hst_allowlisted: bool,
        newallowlisted: bool,
    }

    let q = "
        SELECT
            hst_id,
            hst_host,
            hst_allowlisted,
            IF(
                hst_allowlisted = 0,
                CAST(hst_added_count AS SIGNED) - CAST(hst_removed_count AS SIGNED) > 1000
                AND COUNT(DISTINCT rev_db, rev_page) > 1000,
                CAST(hst_added_count AS SIGNED) - CAST(hst_removed_count AS SIGNED) > 950
                AND COUNT(DISTINCT rev_db, rev_page) > 950
            ) AS newallowlisted
        FROM
            revision,
            addedlink,
            host
        WHERE
            hst_id = al_host
            AND al_rev = rev_id
            AND rev_reverted = 0
            AND rev_page_deleted = 0
            AND (
                CAST(hst_added_count AS SIGNED) - CAST(hst_removed_count AS SIGNED) > 1000
                OR hst_allowlisted = 1
            )
        GROUP BY
            hst_host
        HAVING
            COUNT(DISTINCT rev_db, rev_page) > 1000
            OR hst_allowlisted
        ORDER BY
            hst_id
    ";
    let hosts = query_as::<_, Host>(q).fetch_all(pool).await?;

    for host in hosts
        .iter()
        .filter(|h| h.newallowlisted != h.hst_allowlisted)
    {
        let mut tx = pool.begin().await?;
        let q = "
        UPDATE host SET hst_allowlisted = ? WHERE hst_id = ? AND hst_allowlisted <> ?";
        query(q)
            .bind(host.newallowlisted)
            .bind(host.hst_id)
            .bind(host.newallowlisted)
            .execute(&mut *tx)
            .await?;
        tx.commit().await?;
    }

    let count = hosts.iter().filter(|h| h.newallowlisted).count();
    let added_count = hosts
        .iter()
        .filter(|h| h.newallowlisted && !h.hst_allowlisted)
        .count();
    let removed_count = hosts
        .iter()
        .filter(|h| !h.newallowlisted && h.hst_allowlisted)
        .count();
    info!("{count} hosts allowlisted (+{added_count} -{removed_count}).");
    for host in hosts
        .iter()
        .filter(|h| h.newallowlisted && !h.hst_allowlisted)
    {
        info!("+  {host}", host = unindexify(&host.hst_host));
    }
    for host in hosts
        .iter()
        .filter(|h| !h.newallowlisted && h.hst_allowlisted)
    {
        info!("-  {host}", host = unindexify(&host.hst_host));
    }
    Ok(())
}

#[allow(clippy::too_many_arguments)]
pub async fn insert_http_failure(
    pool: &Pool<MySql>,
    db_id: u16,
    page_id: i64,
    page_title: &str,
    rev_id: i64,
    err_type: &str,
    status: Option<u16>,
    err_text: Option<&str>,
    body: Option<&str>,
) -> Result<(), Error> {
    let mut tx = pool.begin().await?;
    let q = "INSERT INTO httpfailure (hf_api, hf_db, hf_page, hf_title, hf_rev, hf_type, hf_error, hf_status, hf_body) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    query(q)
        .bind("RESTv1")
        .bind(db_id)
        .bind(page_id)
        .bind(page_title)
        .bind(rev_id)
        .bind(err_type)
        .bind(err_text)
        .bind(status)
        .bind(body)
        .execute(&mut *tx)
        .await?;

    tx.commit().await?;
    Ok(())
}

// ---

async fn insert_added_link(
    rev_id: u32,
    scheme_id: u8,
    host_id: u32,
    path: Option<&str>,
    tx: &mut Transaction<'_, MySql>,
) -> Result<(), Error> {
    let path = path.map(|p| split_str(p, 255));
    let (path, path_overflow) = match path {
        None => (None, None),
        Some((path, None)) => (Some(path), None),
        Some((path, Some(rest))) => {
            let q = "INSERT INTO pathoverflow (pof_rest) values (?)";
            let id = u32::try_from(
                query(q)
                    .bind(rest)
                    .execute(&mut **tx)
                    .await?
                    .last_insert_id(),
            )
            .expect("pof_id should fit in u32");
            (Some(path), Some(id))
        }
    };
    let q = "
        INSERT INTO addedlink
        (
            al_rev,
            al_scheme,
            al_host,
            al_path,
            al_path_overflow
        ) values (?, ?, ?, ?, ?)";
    query(q)
        .bind(rev_id)
        .bind(scheme_id)
        .bind(host_id)
        .bind(path)
        .bind(path_overflow)
        .execute(&mut **tx)
        .await?;
    Ok(())
}

#[allow(clippy::too_many_arguments)]
async fn insert_revision(
    rev_ev: &RevisionCreate,
    db_id: u16,
    user_id: Option<i64>,
    global_user_id: Option<u32>,
    ip: Option<&[u8]>,
    ipv4: Option<u32>,
    user_text: Option<u32>,
    tx: &mut Transaction<'_, MySql>,
) -> Result<u32, Error> {
    let q = "
        INSERT INTO revision
        (
            rev_db,
            rev_revision,
            rev_user,
            rev_user_centralauth,
            rev_ipv6,
            rev_ipv4,
            rev_user_text,
            rev_timestamp,
            rev_page,
            rev_namespace,
            rev_minor_edit,
            rev_bot,
            rev_new_page
        )
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    let dt = OffsetDateTime::parse(&rev_ev.rev_timestamp, &Rfc3339)
        .expect("revision timestamp should be RFC3339-compliant");
    let res = query(q)
        .bind(db_id)
        .bind(rev_ev.rev_id)
        .bind(user_id)
        .bind(global_user_id)
        .bind(ip)
        .bind(ipv4)
        .bind(user_text)
        .bind(dt)
        .bind(rev_ev.page_id)
        .bind(rev_ev.page_namespace)
        .bind(rev_ev.rev_minor_edit.unwrap_or_default())
        .bind(
            rev_ev
                .performer
                .as_ref()
                .and_then(|p| p.user_is_bot)
                .unwrap_or_default(),
        )
        .bind(rev_ev.rev_parent_id.is_none())
        .execute(&mut **tx)
        .await?;
    Ok(u32::try_from(res.last_insert_id()).expect("rev_id should fit in u32"))
}

async fn increase_added_count(host_id: u32, tx: &mut Transaction<'_, MySql>) -> Result<(), Error> {
    let q = "
    UPDATE host SET hst_added_count = hst_added_count + 1 WHERE hst_id = ?";
    query(q).bind(host_id).execute(&mut **tx).await?;
    Ok(())
}

async fn increase_removed_count(
    host_id: u32,
    tx: &mut Transaction<'_, MySql>,
) -> Result<(), Error> {
    let q = "
    UPDATE host SET hst_removed_count = hst_removed_count + 1 WHERE hst_id = ?";
    query(q).bind(host_id).execute(&mut **tx).await?;
    Ok(())
}
