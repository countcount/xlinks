CREATE TABLE IF NOT EXISTS tempusername (
    tun_db SMALLINT UNSIGNED NOT NULL,
    tun_rev INT UNSIGNED NOT NULL,
    tun_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (tun_db, tun_rev),
    FOREIGN KEY (tun_db, tun_rev) REFERENCES revision (rev_db, rev_revision)
);
