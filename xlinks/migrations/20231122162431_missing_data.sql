INSERT
    IGNORE INTO usertext (ut_text)
VALUES
    ('(data missing)');

UPDATE
    revision
SET
    rev_user_text = (
        SELECT
            ut_id
        FROM
            usertext
        WHERE
            ut_text = '(data missing)'
    )
WHERE
    rev_user IS NULL
    AND rev_ip IS NULL
    AND rev_user_text IS NULL;
