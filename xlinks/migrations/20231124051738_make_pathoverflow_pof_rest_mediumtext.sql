-- Add migration script here
ALTER TABLE
    pathoverflow
ADD
    COLUMN pof_rest_new MEDIUMTEXT NULL;

UPDATE
    pathoverflow
SET
    pof_rest_new = CONVERT(pof_rest USING utf8);

ALTER TABLE
    pathoverflow DROP COLUMN pof_rest;

ALTER TABLE
    pathoverflow CHANGE pof_rest_new pof_rest MEDIUMTEXT NOT NULL;
