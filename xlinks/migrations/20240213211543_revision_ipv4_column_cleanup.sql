UPDATE revision
SET
    rev_ip = NULL
WHERE
    rev_ipv4 IS NOT NULL
    AND rev_ip IS NOT NULL;

ALTER TABLE revision CHANGE COLUMN rev_ip rev_ipv6 BINARY(16) NULL;
