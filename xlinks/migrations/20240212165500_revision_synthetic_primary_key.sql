ALTER TABLE
    revision
ADD
    COLUMN rev_id INT UNSIGNED NOT NULL;

SET
    @row_number = 0;

UPDATE
    revision
SET
    rev_id = (@row_number := @row_number + 1)
ORDER BY
    rev_timestamp;

ALTER TABLE
    revision
ADD
    CONSTRAINT UNIQUE (rev_db, rev_revision);

ALTER TABLE
    revision DROP CONSTRAINT PRIMARY KEY;

ALTER TABLE
    revision
ADD
    CONSTRAINT PRIMARY KEY (rev_id);

ALTER TABLE
    revision CHANGE COLUMN rev_id rev_id INT UNSIGNED AUTO_INCREMENT;
