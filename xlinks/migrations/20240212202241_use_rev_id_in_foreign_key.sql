ALTER TABLE addedlink
ADD COLUMN al_rev2 INT UNSIGNED NOT NULL;

UPDATE addedlink
JOIN revision ON revision.rev_db = addedlink.al_db
AND revision.rev_revision = addedlink.al_rev
SET
    addedlink.al_rev2 = revision.rev_id;

ALTER TABLE addedlink
DROP FOREIGN KEY addedlink_ibfk_1;

ALTER TABLE addedlink
DROP COLUMN al_db,
DROP COLUMN al_rev;

ALTER TABLE addedlink CHANGE COLUMN al_rev2 al_rev INT UNSIGNED NOT NULL;

ALTER TABLE addedlink ADD FOREIGN KEY (al_rev) REFERENCES revision (rev_id);
