CREATE TABLE IF NOT EXISTS usertext (
    ut_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    ut_text VARCHAR(255) NOT NULL,
    PRIMARY KEY (ut_id),
    UNIQUE KEY (ut_text)
);

CREATE TABLE IF NOT EXISTS userresolve (
    ur_db SMALLINT UNSIGNED NOT NULL,
    ur_rev INT UNSIGNED NOT NULL,
    ur_user INT UNSIGNED NOT NULL,
    ur_text VARCHAR(255) NOT NULL,
    PRIMARY KEY (ur_db, ur_rev),
    FOREIGN KEY (ur_db, ur_rev) REFERENCES revision (rev_db, rev_revision)
);

ALTER TABLE
    revision
ADD
    COLUMN rev_user_text INT UNSIGNED NULL
AFTER
    rev_ip;

ALTER TABLE
    revision
ADD
    FOREIGN KEY (rev_user_text) REFERENCES usertext (ut_id);
