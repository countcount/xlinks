ALTER TABLE
    revision
MODIFY
    COLUMN rev_timestamp TIMESTAMP NOT NULL DEFAULT 0;
