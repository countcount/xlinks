ALTER TABLE
    host
ADD
    COLUMN hst_added_count INT UNSIGNED NULL;

UPDATE
    host h
    LEFT JOIN (
        WITH cte AS (
            SELECT
                hst_id,
                count(*) as c
            FROM
                host,
                addedlink
            WHERE
                al_host = hst_id
            GROUP BY
                hst_id
        )
        SELECT
            *
        FROM
            cte
    ) t ON h.hst_id = t.hst_id
SET
    h.hst_added_count = IF(t.c IS NULL, 0, t.c);

ALTER TABLE
    host
MODIFY
    hst_added_count INT UNSIGNED NOT NULL DEFAULT 0;
