CREATE TABLE
    httpfailure (
        hf_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        hf_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        hf_api VARCHAR(10) NOT NULL,
        hf_type VARCHAR(10) NOT NULL,
        hf_status SMALLINT UNSIGNED NULL,
        hf_body TEXT NULL,
        hf_error TEXT NULL,
        hf_db SMALLINT UNSIGNED NULL,
        hf_rev INT UNSIGNED NULL,
        hf_page INT UNSIGNED NULL,
        hf_title VARCHAR(255) NULL,
        PRIMARY KEY (hf_id),
        FOREIGN KEY (hf_db) REFERENCES db (db_id)
    );
