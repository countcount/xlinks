CREATE TABLE lastprocessed (
    lp_type ENUM('received', 'processed') NOT NULL,
    lp_time TIMESTAMP NOT NULL DEFAULT 0
);

INSERT INTO
    lastprocessed
VALUES
    ('received', 0),
    ('processed', 0);
