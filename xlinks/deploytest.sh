#!/usr/bin/env bash
set -euo pipefail

source ../deploy-common.sh

release_and_deploy xlinkstest xlinks xlinkstest xlinks "toolforge jobs load jobs.yaml"
