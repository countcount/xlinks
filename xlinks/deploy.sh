#!/usr/bin/env bash
set -euo pipefail

source ../deploy-common.sh

release_and_deploy xlinks xlinks xlinks xlinks "toolforge jobs load jobs.yaml"
