DROP DATABASE IF EXISTS s55543__xlinks_p;

CREATE DATABASE s55543__xlinks_p CHARACTER
SET
    utf8mb4 COLLATE utf8mb4_bin;

USE s55543__xlinks_p;

CREATE TABLE
    db (
        db_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
        db_name VARCHAR(32) NOT NULL,
        db_domain VARCHAR(255) NOT NULL,
        PRIMARY KEY (db_id),
        UNIQUE KEY (db_name)
    );

CREATE TABLE
    usertext (
        ut_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        ut_text VARCHAR(255) NOT NULL,
        PRIMARY KEY (ut_id),
        UNIQUE KEY (ut_text)
    );

CREATE TABLE
    revision (
        rev_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        rev_db SMALLINT UNSIGNED NOT NULL,
        rev_revision INT UNSIGNED NOT NULL,
        rev_user INT UNSIGNED NULL,
        rev_user_centralauth INT UNSIGNED NULL,
        rev_ipv4 INT UNSIGNED NULL,
        rev_ipv6 BINARY(16) NULL,
        rev_user_text INT UNSIGNED NULL,
        rev_timestamp TIMESTAMP NOT NULL DEFAULT 0,
        rev_page INT UNSIGNED NOT NULL,
        rev_namespace INT NOT NULL,
        rev_minor_edit TINYINT (1) NOT NULL,
        rev_bot TINYINT (1) NOT NULL,
        rev_new_page TINYINT (1) NOT NULL,
        rev_reverted TINYINT (1) NOT NULL DEFAULT 0,
        rev_page_deleted TINYINT (1) NOT NULL DEFAULT 0,
        PRIMARY KEY (rev_id),
        UNIQUE KEY (rev_db, rev_revision),
        FOREIGN KEY (rev_db) REFERENCES db (db_id),
        FOREIGN KEY (rev_user_text) REFERENCES usertext (ut_id),
        INDEX (rev_db, rev_page),
        INDEX (rev_user_centralauth),
        INDEX (rev_ipv4),
        INDEX (rev_ipv6),
        INDEX (rev_timestamp)
    );

CREATE TABLE
    scheme (
        sch_id TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
        sch_scheme VARCHAR(32) NOT NULL,
        PRIMARY KEY (sch_id),
        UNIQUE KEY (sch_scheme)
    );

CREATE TABLE
    host (
        hst_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        hst_host VARCHAR(255) NOT NULL,
        hst_removed_count INT UNSIGNED NOT NULL DEFAULT 0,
        hst_allowlisted TINYINT (1) NOT NULL DEFAULT 0,
        hst_added_count INT UNSIGNED NOT NULL DEFAULT 0,
        PRIMARY KEY (hst_id),
        UNIQUE (hst_host)
    );

CREATE TABLE
    pathoverflow (
        pof_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        pof_rest MEDIUMTEXT NOT NULL,
        PRIMARY KEY (pof_id)
    );

CREATE TABLE
    addedlink (
        al_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        al_rev INT UNSIGNED NOT NULL,
        al_scheme TINYINT UNSIGNED NOT NULL,
        al_host INT UNSIGNED NOT NULL,
        al_path VARCHAR(255) NULL,
        al_path_overflow INT UNSIGNED NULL,
        PRIMARY KEY (al_id),
        FOREIGN KEY (al_rev) REFERENCES revision (rev_id),
        FOREIGN KEY (al_host) REFERENCES host (hst_id),
        FOREIGN KEY (al_scheme) REFERENCES scheme (sch_id),
        FOREIGN KEY (al_path_overflow) REFERENCES pathoverflow (pof_id),
        INDEX (al_host)
    );

CREATE TABLE
    lastevent (le_ev VARCHAR(1024) NULL);

INSERT INTO
    lastevent (le_ev)
VALUES
    (NULL);

CREATE TABLE
    httpfailure (
        hf_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        hf_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        hf_api VARCHAR(10) NOT NULL,
        hf_type VARCHAR(10) NOT NULL,
        hf_status SMALLINT UNSIGNED NULL,
        hf_body TEXT NULL,
        hf_error TEXT NULL,
        hf_db SMALLINT UNSIGNED NULL,
        hf_rev INT UNSIGNED NULL,
        hf_page INT UNSIGNED NULL,
        hf_title VARCHAR(255) NULL,
        PRIMARY KEY (hf_id),
        FOREIGN KEY (hf_db) REFERENCES db (db_id)
    );
