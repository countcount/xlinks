use std::time::Duration;
use std::{error::Error as StdError, string::FromUtf8Error};

use crate::boxed_clone_sync::BoxCloneSyncService;
use reqwest::header::CONTENT_LENGTH;
use reqwest::{Client, Method, Request, Response, StatusCode, Url};
use tokio::time::sleep;
use tower::{Service, ServiceExt};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("reqwest error")]
    Reqwest(#[from] reqwest::Error),
    #[error("url error")]
    UrlParse(#[from] url::ParseError),
    #[error("UTF8 encoding error")]
    Utf8(#[from] FromUtf8Error),
    #[error("HTTP error status - {status:?} - {body:?}")]
    HttpStatus {
        status: StatusCode,
        body: Option<String>,
    },
    #[error("Unexpected service ready failure: {0}")]
    ServiceReadyFailure(Box<dyn StdError + Send + Sync>),
    #[error("Unexpected service call failure: {0}")]
    ServiceCallFailure(Box<dyn StdError + Send + Sync>),
}

type Result<T> = std::result::Result<T, Error>;

#[allow(clippy::module_name_repetitions)]
pub type ApiClientService = BoxCloneSyncService<
    reqwest::Request,
    reqwest::Response,
    std::boxed::Box<(dyn std::error::Error + std::marker::Send + std::marker::Sync + 'static)>,
>;

pub fn new_rest_api_client_service(user_agent: &str) -> Result<ApiClientService> {
    new_client_service(user_agent, 100, 70, Duration::from_secs(10))
}

pub fn new_action_api_client_service(user_agent: &str) -> Result<ApiClientService> {
    new_client_service(user_agent, 100, 20, Duration::from_secs(10))
}

pub fn new_burstable_action_api_client_service(user_agent: &str) -> Result<ApiClientService> {
    new_client_service(user_agent, 1000, 80, Duration::from_secs(10))
}

pub fn new_sbl_api_client_service(user_agent: &str) -> Result<ApiClientService> {
    new_client_service(user_agent, 25, 70, Duration::from_secs(10))
}

pub async fn get_with_retry(url: &str, service: ApiClientService) -> Result<String> {
    let resp = get_with_retry_resp(url, service).await?;
    response_to_string(resp).await
}

async fn response_to_string(mut resp: Response) -> Result<String> {
    // WP content-length header is trusted
    let len = resp
        .headers()
        .get(CONTENT_LENGTH)
        .and_then(|len| len.to_str().ok())
        .and_then(|s| s.parse::<usize>().ok());
    let mut v = Vec::with_capacity(len.unwrap_or(0));
    loop {
        match resp.chunk().await {
            Ok(Some(chunk)) => v.extend_from_slice(&chunk),
            Ok(None) => break,
            Err(e) => return Err(Error::Reqwest(e)),
        }
    }
    Ok(String::from_utf8(v)?)
}

pub async fn get_with_retry_resp(url: &str, mut service: ApiClientService) -> Result<Response> {
    const DEBUG: bool = false;
    static REQUEST_COUNT: std::sync::atomic::AtomicUsize = std::sync::atomic::AtomicUsize::new(0);
    static RETIRED_COUNT: std::sync::atomic::AtomicUsize = std::sync::atomic::AtomicUsize::new(0);
    let mut try_no = 0;
    loop {
        let request = Request::new(Method::GET, Url::parse(url)?);
        let ready_service = service.ready().await.map_err(Error::ServiceReadyFailure)?;
        if DEBUG {
            REQUEST_COUNT.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
            eprintln!(
                "Request count: {}",
                REQUEST_COUNT.load(std::sync::atomic::Ordering::Relaxed)
            );
        }
        let response =
            ready_service
                .call(request)
                .await
                .map_err(|e| match e.downcast::<reqwest::Error>() {
                    Ok(re) => Error::Reqwest(*re),
                    Err(e) => Error::ServiceCallFailure(e),
                });
        let res = match response {
            Ok(response) => {
                let status = response.status();
                if status.is_client_error() || status.is_server_error() {
                    Err(Error::HttpStatus {
                        status,
                        body: response_to_string(response).await.ok(),
                    })
                } else {
                    Ok(response)
                }
            }
            Err(e) => Err(e),
        };
        if DEBUG {
            RETIRED_COUNT.fetch_add(1, std::sync::atomic::Ordering::Relaxed);
            eprintln!(
                "Retired count: {}",
                RETIRED_COUNT.load(std::sync::atomic::Ordering::Relaxed)
            );
        }
        if try_no < 2 {
            if let Err(e) = res {
                if let Error::HttpStatus {
                    status: status_code,
                    body: _,
                } = e
                {
                    match status_code {
                        StatusCode::TOO_MANY_REQUESTS => {
                            if DEBUG {
                                eprintln!("Too many requests, retrying...");
                            }
                            // extra sleep, then retry
                            sleep(Duration::from_secs_f64(
                                rand::random::<f64>().mul_add(10.0, 30.0),
                            ))
                            .await;
                        }
                        _ if status_code.is_server_error() => {
                            // retry
                        }
                        _ => {
                            // client errors, (unexpected) redirects, informational
                            return Err(e);
                        }
                    }
                }
                // also retry on connection/timeout/... errors
                try_no += 1;
                sleep(Duration::from_secs_f64(
                    rand::random::<f64>().mul_add(5.0, 10.0),
                ))
                .await;
                continue;
            }
        }
        return res;
    }
}

fn new_client_service(
    user_agent: &str,
    rate_limit_per_sec: u64,
    concurrency_limit: usize,
    pool_idle_timeout: Duration,
) -> Result<ApiClientService> {
    let client = Client::builder()
        .user_agent(user_agent)
        .timeout(Duration::from_secs(120))
        .pool_max_idle_per_host(1)
        .pool_idle_timeout(pool_idle_timeout)
        .build()?;

    let service = tower::ServiceBuilder::new()
        .buffer(100)
        .rate_limit(rate_limit_per_sec, Duration::from_secs(1))
        .concurrency_limit(concurrency_limit)
        .service(client);
    Ok(BoxCloneSyncService::new(service))
}
