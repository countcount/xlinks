pub mod api_client;
pub mod boxed_clone_sync;

use std::fmt::Write;
use std::net::{Ipv4Addr, Ipv6Addr};
use std::str::FromStr;
use url::Url;

#[must_use]
pub fn normalize_host(host: &str) -> Option<String> {
    if !host.contains("xn--")
        && host
            .chars()
            .all(|c| c.is_ascii_lowercase() || c.is_ascii_digit() || c == '-' || c == '.')
    {
        return None;
    }
    let mut host_url = String::with_capacity(8 + host.len());
    host_url.push_str("https://");
    host_url.push_str(host);
    let host_url = Url::parse(&host_url);
    let host = host_url
        .as_ref()
        .map(|u| u.host_str())
        .unwrap_or(Some(host)) // TODO: allows adding hosts with urlencoded characters
        .unwrap_or(host);
    Some(idna::domain_to_unicode(host).0)
}

#[must_use]
pub fn indexify(host: &str) -> String {
    if Ipv4Addr::from_str(host).is_ok() {
        return format!("V4.{host}") + ".";
    } else if host.starts_with('[') && host.ends_with(']') {
        if let Ok(ip) = Ipv6Addr::from_str(&host[1..host.len() - 1]) {
            return format!("V6.{}", ipv6_unabbreviated(ip)) + ".";
        }
    }
    host.split('.').rev().collect::<Vec<&str>>().join(".") + "."
}

#[must_use]
pub fn unindexify(host: &str) -> String {
    if host.starts_with("V4.") {
        if let Ok(ip_addr) = Ipv4Addr::from_str(&host[3..host.len() - 1]) {
            return ip_addr.to_string();
        }
    }
    if host.starts_with("V6.") {
        if let Ok(ip_addr) = Ipv6Addr::from_str(
            host[3..host.len() - 1]
                .to_string()
                .replace('.', ":")
                .as_str(),
        ) {
            return "[".to_string() + &ip_addr.to_string() + "]";
        }
    }
    let mut res = host.split('.').rev().collect::<Vec<&str>>().join(".");
    if res.get(0..1) == Some(".") {
        res.remove(0);
    }
    res
}
fn ipv6_unabbreviated(ip: Ipv6Addr) -> String {
    let mut res = String::with_capacity(39);
    for s in ip.segments() {
        write!(res, "{s:X}:").expect("writing to a String never fails");
    }
    res
}
