use std::sync::Arc;
use std::time::Duration;

use futures::{StreamExt, future::try_join_all, stream::FuturesUnordered};
use sqlx::{
    ConnectOptions, MySql, Pool,
    mysql::{MySqlConnectOptions, MySqlPoolOptions},
    pool::PoolConnection,
};
use tracing::log::LevelFilter;

#[derive(Clone)]
pub struct ReplicaPools {
    pools: Arc<Vec<Pool<MySql>>>,
    wikis: Arc<Vec<Wiki>>,
}

pub const SHARD_COUNT: usize = 8;

const WEB_SHARD_URLS: [&str; SHARD_COUNT] = [
    "mysql://s1.web.db.svc.wikimedia.cloud",
    "mysql://s2.web.db.svc.wikimedia.cloud",
    "mysql://s3.web.db.svc.wikimedia.cloud",
    "mysql://s4.web.db.svc.wikimedia.cloud",
    "mysql://s5.web.db.svc.wikimedia.cloud",
    "mysql://s6.web.db.svc.wikimedia.cloud",
    "mysql://s7.web.db.svc.wikimedia.cloud",
    "mysql://s8.web.db.svc.wikimedia.cloud",
];

const ANALYTICS_SHARD_URLS: [&str; SHARD_COUNT] = [
    "mysql://s1.analytics.db.svc.wikimedia.cloud",
    "mysql://s2.analytics.db.svc.wikimedia.cloud",
    "mysql://s3.analytics.db.svc.wikimedia.cloud",
    "mysql://s4.analytics.db.svc.wikimedia.cloud",
    "mysql://s5.analytics.db.svc.wikimedia.cloud",
    "mysql://s6.analytics.db.svc.wikimedia.cloud",
    "mysql://s7.analytics.db.svc.wikimedia.cloud",
    "mysql://s8.analytics.db.svc.wikimedia.cloud",
];

#[derive(Clone, Debug, sqlx::FromRow)]
#[allow(dead_code)]
pub struct Wiki {
    pub dbname: String,
    pub shard: String,
    pub lang: String,
    pub family: String,
    pub url: String,
}

#[derive(Clone, Copy, Debug)]
pub struct Shard(u8);

impl From<Shard> for usize {
    fn from(shard: Shard) -> Self {
        shard.0 as Self
    }
}

impl TryFrom<usize> for Shard {
    type Error = Error;

    fn try_from(value: usize) -> Result<Self, Self::Error> {
        if (0..SHARD_COUNT).contains(&value) {
            Ok(Self(u8::try_from(value).expect("shard value must be u8")))
        } else {
            Err(Error::ShardNotFound(value))
        }
    }
}

const META_SHARD: Shard = Shard(6);

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("database error")]
    Database(#[from] sqlx::Error),
    #[error("wiki not found {0}")]
    WikiNotFound(String),
    #[error("shard not found {0}")]
    ShardNotFound(usize),
}

#[derive(Debug)]
pub struct Builder {
    shard_urls: Vec<String>,
    max_connections: u32,
    idle_timeout: Duration,
    slow_statement_threshold: Option<Duration>,
    disable_statement_logging: bool,
}

impl Builder {
    #[must_use]
    pub fn new() -> Self {
        Self {
            shard_urls: ANALYTICS_SHARD_URLS
                .iter()
                .map(|s| (*s).to_string())
                .collect(),
            max_connections: 3,
            idle_timeout: Duration::from_secs(1),
            slow_statement_threshold: None,
            disable_statement_logging: false,
        }
    }

    #[must_use]
    pub fn with_custom_connection(mut self, shard_urls: [String; SHARD_COUNT]) -> Self {
        self.shard_urls = shard_urls.into_iter().collect();
        self
    }

    #[must_use]
    pub fn with_analytics_db(mut self) -> Self {
        self.shard_urls = ANALYTICS_SHARD_URLS
            .iter()
            .map(|s| (*s).to_string())
            .collect();
        self
    }

    #[must_use]
    pub fn with_web_db(mut self) -> Self {
        self.shard_urls = WEB_SHARD_URLS.iter().map(|s| (*s).to_string()).collect();
        self
    }

    #[must_use]
    pub fn with_max_connections(mut self, max: u32) -> Self {
        assert!(max > 0, "max connections must be greater than 0");
        self.max_connections = max;
        self
    }

    #[must_use]
    pub const fn with_slow_statement_threshold(mut self, threshold: Duration) -> Self {
        self.slow_statement_threshold = Some(threshold);
        self
    }

    #[must_use]
    pub const fn disable_statement_logging(mut self, disable: bool) -> Self {
        self.disable_statement_logging = disable;
        self
    }

    pub async fn build(self, username: &str, password: &str) -> Result<ReplicaPools, Error> {
        ReplicaPools::new(
            self.shard_urls,
            self.max_connections,
            self.idle_timeout,
            self.slow_statement_threshold,
            self.disable_statement_logging,
            username,
            password,
        )
        .await
    }
}

impl Default for Builder {
    fn default() -> Self {
        Self::new()
    }
}

impl ReplicaPools {
    async fn new(
        shard_urls: Vec<String>,
        max_connections: u32,
        idle_timeout: Duration,
        slow_statement_threshold: Option<Duration>,
        disable_statement_logging: bool,
        username: &str,
        password: &str,
    ) -> Result<Self, Error> {
        let mut pool_futures = Vec::with_capacity(shard_urls.len());
        for url in shard_urls {
            let mut conn = url
                .parse::<MySqlConnectOptions>()?
                .username(username)
                .password(password);
            if let Some(slow_statement_threshold) = slow_statement_threshold {
                conn = conn.log_slow_statements(LevelFilter::Warn, slow_statement_threshold);
            }
            if disable_statement_logging {
                conn = conn.disable_statement_logging();
            }

            let pool_future = MySqlPoolOptions::new()
                .max_connections(max_connections)
                .min_connections(0)
                .idle_timeout(idle_timeout)
                .connect_with(conn);
            pool_futures.push(pool_future);
        }
        let pools = try_join_all(pool_futures).await?;

        let wikis = sqlx::query_as::<_, Wiki>(
            "SELECT dbname, slice as shard, name, lang, family, url FROM meta_p.wiki WHERE is_closed<>1 ORDER BY slice ASC",
        )
        .fetch_all(&pools[usize::from(META_SHARD)])
        .await?;

        Ok(Self {
            pools: pools.into(),
            wikis: wikis.into(),
        })
    }

    #[must_use]
    pub fn all_wikis(&self) -> Arc<Vec<Wiki>> {
        self.wikis.clone()
    }

    #[must_use]
    pub fn get_shard_count(&self) -> usize {
        self.pools.len()
    }

    pub fn get_shard(&self, wiki: &str) -> Result<Shard, Error> {
        let shard = self
            .wikis
            .iter()
            .find(|w| w.dbname == wiki)
            .map(|w| extract_shard_no(&w.shard))
            .ok_or(Error::WikiNotFound(wiki.to_string()))?;
        Ok(shard)
    }

    pub async fn acquire_for_shard(
        &self,
        shard: Shard,
    ) -> Result<PoolConnection<MySql>, sqlx::Error> {
        self.pools[usize::from(shard)].acquire().await
    }

    pub async fn acquire_for_wiki(&self, wiki: &str) -> Result<PoolConnection<MySql>, Error> {
        Ok(self.pools[self.get_shard(wiki)?.0 as usize]
            .acquire()
            .await?)
    }

    pub async fn acquire_for_centralauth(&self) -> Result<PoolConnection<MySql>, sqlx::Error> {
        self.acquire_for_shard(META_SHARD).await
    }

    pub async fn close(&self) {
        self.pools
            .iter()
            .map(|p| p.close())
            .collect::<FuturesUnordered<_>>()
            .for_each_concurrent(None, |()| async {})
            .await;
    }
}

fn extract_shard_no(slice: &str) -> Shard {
    if slice.starts_with('s') && slice.ends_with(".labsdb") && slice.len() == 9 {
        let shard = slice[1..2].parse::<usize>().expect("invalid shard");
        if (1..=SHARD_COUNT).contains(&shard) {
            return Shard(u8::try_from(shard - 1).expect("shard value must be u8"));
        }
    }
    panic!("invalid shard");
}
