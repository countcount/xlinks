release_and_deploy() {
  TAGPREFIX=$1
  BINARY=$2
  TOOL_ACCT=$3
  BINARY_PATH=$4
  RESTART_CMD=$5

  if [[ $TAGPREFIX != "xlinkstest" && `git rev-parse --abbrev-ref HEAD` != "main" ]]; then echo Not on main branch && exit 1; fi

  if [[ `git status --porcelain` ]]; then echo Repository has local changes && exit 1; fi

  for i in {1..99}
  do
    TAG=$TAGPREFIX-$(date '+%Y%m%d')$(printf "%02d\n" $i)
    if ! git rev-parse $TAG >/dev/null 2>&1; then
      break
    fi
  done

  echo Creating release $TAG...
  git tag $TAG
  git push -q origin main $TAG
  while true; do
    STATUS=$(curl -f -s https://gitlab.wikimedia.org/api/v4/projects/countcount%2Fxlinks/pipelines?ref=$TAG|jq .[0].status)
    case $STATUS in
      '"success"')
        break
        ;;
      '"failed"')
        echo CI failed to build release $TAG
        exit 1
        ;;
      *)
        ;;
    esac
    sleep 1
  done

  URL=https://gitlab.wikimedia.org/countcount/xlinks/-/releases/$TAG/downloads/binaries/$BINARY.gz

  echo Deploying...
  ssh login.toolforge.org "set -euo pipefail
  become $TOOL_ACCT curl -s -S -f -L -O $URL
  become $TOOL_ACCT bash -c 'gunzip -c $BINARY.gz > $BINARY_PATH'
  become $TOOL_ACCT chmod +x $BINARY_PATH
  become $TOOL_ACCT $RESTART_CMD
  echo Done."
}
