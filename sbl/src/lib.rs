use std::collections::BTreeMap;
use std::sync::Arc;
use std::sync::RwLock;

use futures::TryStreamExt;
use futures::stream::{self};
use http::StatusCode;
use serde_json::Value;
use shared::api_client;
use shared::api_client::ApiClientService;
use shared::api_client::get_with_retry;
use tracing::info;
use url::Url;
use wikimedia_events::page_change::PageChange;

// TODO
// - multithreading

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("API client error")]
    Database(#[from] shared::api_client::Error),
    #[error("Sitematrix parse error: {0}")]
    SitematrixParse(&'static str),
    #[error("JSON parse error")]
    Json(#[from] serde_json::Error),
    #[error("Sitematrix parse error: {0}")]
    RestParse(&'static str),
    #[error("PCRE2 regex match error")]
    RegexMatchError(#[from] pcre2::Error),
}

#[derive(Debug, Eq, PartialEq)]
pub enum Hit {
    GlobalSpamBlacklist {
        line_no: usize,
        pattern: String,
    },
    SpamBlacklist {
        wiki_domain: String,
        line_no: usize,
        pattern: String,
    },
    SpamWhitelist {
        wiki_domain: String,
        line_no: usize,
        pattern: String,
    },
    BlockedExternalDomain {
        wiki_domain: String,
        blocked_domain: String,
        notes: String,
        added_by: Option<String>,
    },
}

#[derive(Clone)]
pub struct SblCheck {
    inner: Arc<RwLock<SblCheckInner>>,
}

struct SblCheckInner {
    global_sbl: Vec<LinePattern>,
    sbls: BTreeMap<String, Vec<LinePattern>>,
    swls: BTreeMap<String, Vec<LinePattern>>,
    beds: BTreeMap<String, Vec<BEDEntry>>,
}

struct LinePattern {
    regex: pcre2::bytes::Regex,
    line_no: usize,
}

#[derive(serde::Deserialize)]
struct BEDEntry {
    domain: String,
    notes: String,
    #[serde(rename = "addedBy")]
    added_by: Option<String>,
}

type Result<T> = std::result::Result<T, Error>;

impl SblCheck {
    #[must_use]
    pub fn empty() -> Self {
        Self {
            inner: Arc::new(RwLock::new(SblCheckInner {
                global_sbl: Vec::new(),
                sbls: BTreeMap::new(),
                swls: BTreeMap::new(),
                beds: BTreeMap::new(),
            })),
        }
    }

    pub async fn new(service: &ApiClientService) -> Result<Self> {
        let self0 = Self::empty();
        self0.update_all(service).await?;
        Ok(self0)
    }

    pub async fn update_all(&self, service: &ApiClientService) -> Result<()> {
        self.update_global_sbl(service).await?;
        let wdomains = get_wikis(&get_sitematrix_json(service).await?)?;
        let stream = stream::iter(wdomains.into_iter().map(Ok::<_, Error>));
        stream
            .try_for_each_concurrent(100, |wdomain| async move {
                self.update_site_sbl(&wdomain, service).await?;
                self.update_site_swl(&wdomain, service).await?;
                self.update_site_beds(&wdomain, service).await?;
                Ok(())
            })
            .await?;
        Ok(())
    }

    pub fn get_hits(&self, qurl: &str) -> Result<Vec<Hit>> {
        let Normalized {
            ascii_url,
            unicode_host,
        } = normalize(qurl);

        let mut res = Vec::new();
        let inner = self.inner.read().expect("lock should never be poisoned");
        add_global_sbl_hits(&inner.global_sbl, &ascii_url, &mut res)?;
        for (k, v) in &inner.sbls {
            add_sbl_hits(v, &ascii_url, &mut res, k)?;
        }
        for (k, v) in &inner.swls {
            add_swl_hits(v, &ascii_url, &mut res, k)?;
        }
        if let Some(unicode_host) = unicode_host {
            for (k, v) in &inner.beds {
                add_bed_hits(v, &unicode_host, &mut res, k);
            }
        }
        Ok(res)
    }

    pub async fn page_changed(&self, ev: &PageChange, service: &ApiClientService) -> Result<()> {
        let (Some(domain), Some(namespace)) = (ev.meta.domain.as_ref(), ev.page.namespace_id)
        else {
            return Ok(());
        };
        self.update_from_event(domain, namespace, &ev.page.page_title, service)
            .await?;
        Ok(())
    }

    async fn update_from_event(
        &self,
        wdomain: &str,
        ns: i64,
        title: &str,
        service: &ApiClientService,
    ) -> Result<()> {
        if wdomain == "meta.wikimedia.org" && ns == 0 && title == "Spam_blacklist" {
            info!("Updating global spam blacklist");
            self.update_global_sbl(service).await?;
        } else if ns == 8 {
            let Some(title) = title.split(':').nth(1) else {
                return Ok(());
            };
            if title == "BlockedExternalDomains.json" {
                info!("Updating {wdomain} BlockedExternalDomains");
                self.update_site_beds(wdomain, service).await?;
            } else if title == "Spam-blacklist" {
                info!("Updating {wdomain} spam blacklist");
                self.update_site_sbl(wdomain, service).await?;
            } else if title == "Spam-whitelist" {
                info!("Updating {wdomain} spam whitelist");
                self.update_site_swl(wdomain, service).await?;
            }
        }
        Ok(())
    }

    async fn update_global_sbl(&self, service: &ApiClientService) -> Result<()> {
        let sbl_entries = get_patterns(rest_global_sbl_url(), service).await?;
        let mut inner = self.inner_write();
        inner.global_sbl = sbl_entries;
        Ok(())
    }

    fn inner_write(&self) -> std::sync::RwLockWriteGuard<SblCheckInner> {
        self.inner.write().expect("lock should never be poisoned")
    }

    async fn update_site_sbl(&self, wdomain: &str, service: &ApiClientService) -> Result<()> {
        let sbl_entries = get_patterns(&rest_sbl_url(wdomain), service).await?;
        if sbl_entries.is_empty() {
            self.inner_write().sbls.remove(wdomain);
        } else {
            self.inner_write()
                .sbls
                .insert(wdomain.to_string(), sbl_entries);
        }
        Ok(())
    }

    async fn update_site_swl(&self, wdomain: &str, service: &ApiClientService) -> Result<()> {
        let swl_entries = get_patterns(&rest_swl_url(wdomain), service).await?;
        if swl_entries.is_empty() {
            self.inner_write().swls.remove(wdomain);
        } else {
            self.inner_write()
                .swls
                .insert(wdomain.to_string(), swl_entries);
        }
        Ok(())
    }

    async fn update_site_beds(&self, wdomain: &str, service: &ApiClientService) -> Result<()> {
        let bed_entries = get_bed_entries(&rest_bed_url(wdomain), service).await?;
        if bed_entries.is_empty() {
            self.inner_write().beds.remove(wdomain);
        } else {
            self.inner_write()
                .beds
                .insert(wdomain.to_string(), bed_entries);
        }
        Ok(())
    }
}

impl Hit {
    #[must_use]
    pub fn wiki_domain(&self) -> Option<&str> {
        match self {
            Self::GlobalSpamBlacklist { .. } => None,
            Self::SpamBlacklist { wiki_domain, .. }
            | Self::SpamWhitelist { wiki_domain, .. }
            | Self::BlockedExternalDomain { wiki_domain, .. } => Some(wiki_domain),
        }
    }

    #[must_use]
    pub fn source_url(&self) -> String {
        match self {
            Self::GlobalSpamBlacklist { .. } => {
                "https://meta.wikimedia.org/wiki/Spam_blacklist".to_string()
            }
            Self::SpamBlacklist { wiki_domain, .. } => {
                format!("https://{wiki_domain}/wiki/MediaWiki:Spam-blacklist")
            }
            Self::SpamWhitelist { wiki_domain, .. } => {
                format!("https://{wiki_domain}/wiki/MediaWiki:Spam-whitelist")
            }
            Self::BlockedExternalDomain { wiki_domain, .. } => {
                format!("https://{wiki_domain}/wiki/Special:BlockedExternalDomains")
            }
        }
    }
}

struct Normalized {
    ascii_url: String,
    unicode_host: Option<String>,
}

/// Returns the URL with the host in normalized ASCII form and the host in normalized unicode form.
/// If the URL is invalid, the function returns the URL as is and None for the unicode host.
fn normalize(url: &str) -> Normalized {
    let Ok(url) = Url::parse(url) else {
        return Normalized {
            ascii_url: url.to_string(),
            unicode_host: None,
        };
    };

    Normalized {
        ascii_url: url.to_string(),
        unicode_host: url.host_str().map(|h| idna::domain_to_unicode(h).0),
    }
}

fn add_global_sbl_hits(sbl_lines: &Vec<LinePattern>, qurl: &str, res: &mut Vec<Hit>) -> Result<()> {
    for l in sbl_lines {
        // clone to immediately free scratch space
        if l.regex.clone().is_match(qurl.as_bytes())? {
            res.push(Hit::GlobalSpamBlacklist {
                line_no: l.line_no,
                pattern: l.regex.as_str().to_string(),
            });
        }
    }
    Ok(())
}

fn add_sbl_hits(
    sbl_lines: &Vec<LinePattern>,
    qurl: &str,
    res: &mut Vec<Hit>,
    wdomain: &str,
) -> Result<()> {
    for l in sbl_lines {
        // clone to immediately free scratch space
        if l.regex.clone().is_match(qurl.as_bytes())? {
            res.push(Hit::SpamBlacklist {
                wiki_domain: wdomain.to_string(),
                line_no: l.line_no,
                pattern: l.regex.as_str().to_string(),
            });
        }
    }
    Ok(())
}

fn add_swl_hits(
    swl_lines: &Vec<LinePattern>,
    qurl: &str,
    res: &mut Vec<Hit>,
    wdomain: &str,
) -> Result<()> {
    for l in swl_lines {
        // clone to immediately free scratch space
        if l.regex.clone().is_match(qurl.as_bytes())? {
            res.push(Hit::SpamWhitelist {
                wiki_domain: wdomain.to_string(),
                line_no: l.line_no,
                pattern: l.regex.as_str().to_string(),
            });
        }
    }
    Ok(())
}

fn add_bed_hits(v: &[BEDEntry], domain: &str, res: &mut Vec<Hit>, wdomain: &str) {
    for entry in v {
        if domain.ends_with(&entry.domain)
            && (domain.len() == entry.domain.len()
                || domain.as_bytes()[domain.len() - entry.domain.len() - 1] == b'.')
        {
            res.push(Hit::BlockedExternalDomain {
                wiki_domain: wdomain.to_string(),
                blocked_domain: entry.domain.clone(),
                notes: entry.notes.clone(),
                added_by: entry.added_by.clone(),
            });
        }
    }
}

fn rest_sbl_url(wdomain: &str) -> String {
    format!("https://{wdomain}/w/rest.php/v1/page/MediaWiki:Spam-blacklist")
}

fn rest_swl_url(wdomain: &str) -> String {
    format!("https://{wdomain}/w/rest.php/v1/page/MediaWiki:Spam-whitelist")
}

fn rest_bed_url(wdomain: &str) -> String {
    format!("https://{wdomain}/w/rest.php/v1/page/MediaWiki:BlockedExternalDomains.json")
}

fn rest_global_sbl_url() -> &'static str {
    "https://meta.wikimedia.org/w/rest.php/v1/page/Spam_blacklist"
}

async fn get_patterns(url: &str, service: &ApiClientService) -> Result<Vec<LinePattern>> {
    let Some(contents) = get_contents(url, service).await? else {
        return Ok(Vec::new());
    };

    let mut res = Vec::new();
    for (line_no, line) in contents.lines().enumerate() {
        let line = line.split('#').next().expect("never fails").trim();
        if line.is_empty() {
            continue;
        }
        if let Ok(regex) = pcre2::bytes::Regex::new(line) {
            res.push(LinePattern {
                regex,
                line_no: line_no + 1,
            });
        }
    }
    Ok(res)
}

async fn get_bed_entries(url: &str, service: &ApiClientService) -> Result<Vec<BEDEntry>> {
    let Some(contents) = get_contents(url, service).await? else {
        return Ok(Vec::new());
    };

    Ok(serde_json::from_str::<Vec<BEDEntry>>(&contents)?)
}

async fn get_contents(url: &str, service: &ApiClientService) -> Result<Option<String>> {
    let contents = match get_with_retry(url, service.clone()).await {
        Ok(contents) => contents,
        Err(api_client::Error::HttpStatus {
            status: StatusCode::NOT_FOUND,
            body: _,
        }) => {
            // ignore
            return Ok(None);
        }
        Err(e) => {
            return Err(e.into());
        }
    };
    let json = serde_json::from_str::<Value>(&contents)?;
    let contents = json
        .as_object()
        .ok_or(Error::RestParse("REST API not JSON"))?
        .get("source")
        .ok_or(Error::RestParse("source field not found"))?
        .as_str()
        .ok_or(Error::RestParse("source not a string"))?;
    Ok(Some(contents.to_string()))
}

async fn get_sitematrix_json(service: &ApiClientService) -> Result<String> {
    Ok(get_with_retry(
        "https://meta.wikimedia.org/w/api.php?action=sitematrix&smsiteprop=url&format=json",
        service.clone(),
    )
    .await?)
}

fn get_wikis(sitematrix: &str) -> Result<Vec<String>> {
    let json: Value = serde_json::from_str(sitematrix)?;

    let mut domains = Vec::new();
    let Some(sitematrix) = json.get("sitematrix").and_then(|s| s.as_object()) else {
        return Err(Error::SitematrixParse(
            "sitematrix not found or not an object",
        ));
    };
    for (key, value) in sitematrix {
        if key == "count" {
            continue;
        }

        let sites = if key == "specials" {
            value
                .as_array()
                .ok_or(Error::SitematrixParse("specials not an array"))?
        } else {
            value
                .get("site")
                .and_then(|s| s.as_array())
                .ok_or(Error::SitematrixParse("site not found or not an array"))?
        };

        for site in sites {
            let Some(url) = site.get("url").and_then(|u| u.as_str()) else {
                eprintln!("url not found or not a string");
                continue;
            };
            let private = site.get("private").is_some_and(|v| v.as_str() == Some(""));

            // let closed = site.get("closed").map_or(false, |v| v.as_str() == Some(""));

            if !private {
                let url =
                    &Url::parse(url).map_err(|_| Error::SitematrixParse("wiki url is invalid"))?;
                let domain = url
                    .host_str()
                    .ok_or(Error::SitematrixParse("wiki url has no host"))?;
                if domain == "labtestwikitech.wikimedia.org" {
                    continue;
                }
                domains.push(domain.to_string());
            }
        }
    }
    Ok(domains)
}

#[cfg(test)]
mod tests {
    use shared::api_client::new_sbl_api_client_service;

    use super::*;

    #[tokio::test]
    async fn get_sitematrix_works() -> Result<()> {
        let service = new_sbl_api_client_service("test")?;
        let _ = get_sitematrix_json(&service).await?;
        Ok(())
    }

    #[tokio::test]
    async fn parsing_sitematrix_works() -> Result<()> {
        let service = new_sbl_api_client_service("test")?;
        let sm = get_sitematrix_json(&service).await?;
        let sites = get_wikis(&sm)?;
        assert!(sites.iter().any(|x| x == "en.wikipedia.org"));
        assert!(sites.iter().any(|x| x == "meta.wikimedia.org"));
        assert!(!sites.iter().any(|x| x == "labtestwikitech.wikimedia.org"));
        assert!(sites.len() > 900);
        Ok(())
    }

    // slow test
    #[ignore]
    #[tokio::test]
    async fn test_not_blacklisted() -> Result<()> {
        let service = new_sbl_api_client_service("test")?;
        let sblc = SblCheck::new(&service).await?;
        assert!(sblc.get_hits("https://www.google.com")?.is_empty());
        Ok(())
    }

    #[tokio::test]
    async fn test_enwiki_sbl() -> Result<()> {
        let service = new_sbl_api_client_service("test")?;
        let sblc = SblCheck::empty();
        sblc.update_site_sbl("en.wikipedia.org", &service).await?;

        // no match
        let urls = ["https://www.google.com", "https://www.some-change.org"];
        for url in &urls {
            assert!(sblc.get_hits(url)?.is_empty());
        }

        // exact match
        let hits = sblc.get_hits("www.01bags.com")?;
        assert_eq!(
            hits,
            vec![Hit::SpamBlacklist {
                wiki_domain: "en.wikipedia.org".to_string(),
                line_no: 19,
                pattern: r"\b01bags\.com\b".to_string(),
            }]
        );
        assert_eq!(hits[0].wiki_domain(), Some("en.wikipedia.org"));
        assert_eq!(
            hits[0].source_url(),
            "https://en.wikipedia.org/wiki/MediaWiki:Spam-blacklist",
        );

        Ok(())
    }

    #[tokio::test]
    async fn test_enwiki_swl() -> Result<()> {
        let service = new_sbl_api_client_service("test")?;
        let sblc = SblCheck::empty();
        sblc.update_global_sbl(&service).await?;
        sblc.update_site_swl("en.wikipedia.org", &service).await?;

        // no match
        let urls = ["https://www.google.com", "https://www.some-change.org"];
        for url in &urls {
            assert!(sblc.get_hits(url)?.is_empty());
        }

        // exact match
        let hits = sblc.get_hits("elainecarroll.xyz")?;
        assert_eq!(
            hits,
            vec![Hit::SpamWhitelist {
                wiki_domain: "en.wikipedia.org".to_string(),
                line_no: 954,
                pattern: r"\belainecarroll\.xyz\b".to_string(),
            }]
        );
        assert_eq!(hits[0].wiki_domain(), Some("en.wikipedia.org"));
        assert_eq!(
            hits[0].source_url(),
            "https://en.wikipedia.org/wiki/MediaWiki:Spam-whitelist",
        );

        Ok(())
    }

    #[tokio::test]
    async fn test_enwiki_beds() -> Result<()> {
        let service = new_sbl_api_client_service("test")?;
        let sblc = SblCheck::empty();
        sblc.update_site_beds("en.wikipedia.org", &service).await?;

        // no match
        let urls = ["https://www.google.com"];
        for url in &urls {
            assert!(sblc.get_hits(url)?.is_empty());
        }

        // single match
        let urls = [
            "https://testinvalid.invalid",
            "https://www.testinvalid.invalid",
            "https://testInvalid.inValid",
        ];
        for url in &urls {
            assert!(sblc.get_hits(url)?.len() == 1);
        }

        // exact match
        let hits = sblc.get_hits("https://testinvalid.invalid")?;
        assert_eq!(
            hits,
            vec![Hit::BlockedExternalDomain {
                wiki_domain: "en.wikipedia.org".to_string(),
                blocked_domain: "testinvalid.invalid".to_string(),
                notes: "Test domain -- Xaosflux 11:50, 25 June 2023 (UTC)".to_string(),
                added_by: Some("Xaosflux".to_string())
            }]
        );
        assert_eq!(hits[0].wiki_domain(), Some("en.wikipedia.org"));
        assert_eq!(
            hits[0].source_url(),
            "https://en.wikipedia.org/wiki/Special:BlockedExternalDomains",
        );

        Ok(())
    }

    #[tokio::test]
    async fn test_global_sbl() -> Result<()> {
        let service = new_sbl_api_client_service("test")?;
        let sblc = SblCheck::empty();
        sblc.update_global_sbl(&service).await?;

        // no match
        let urls = ["https://www.google.com", "https://www.some-change.org"];
        for url in &urls {
            assert!(sblc.get_hits(url)?.is_empty());
        }

        // single match
        let urls = ["https://www.khansays.com", "https://abc-five88-def.org"];
        for url in &urls {
            assert!(sblc.get_hits(url)?.len() == 1);
        }

        // exact match
        let hits = sblc.get_hits("www.a-s-e-x-b.com")?;
        assert_eq!(
            hits,
            vec![Hit::GlobalSpamBlacklist {
                line_no: 19,
                pattern: "s-e-x".to_string(),
            }]
        );
        assert_eq!(hits[0].wiki_domain(), None);
        assert_eq!(
            hits[0].source_url(),
            "https://meta.wikimedia.org/wiki/Spam_blacklist",
        );
        Ok(())
    }
}
